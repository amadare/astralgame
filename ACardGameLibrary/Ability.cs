﻿namespace ACardGameEngine {
    public abstract class Ability {
        public AbilityType Type;
        public CreatureCard Card, Target;
        public Player Owner, Enemy;
        public int Cost, Value;

        public Ability(CreatureCard card, Player owner, Player enemy, int cost) {
            Card = card;
            Owner = owner;
            Enemy = enemy;
            Cost = cost;
        }

        public abstract void Apply();
    }
}