﻿using System;

namespace ACardGameEngine {
    public abstract class Card {
        //назва пікторгамки для карти, яку буде 
        //завантажувати менеджер бази данних
        public string PictureName;

        //назва і опис карти
        public readonly string Name, Description;

        //основні х-ки карти, вартість покупки, к-ть мани, необхідної для активації
        public int ManaCost,GoldWorth;

        //вказує на позицію карти на столі. Приймає значення -1, якщо карта в руці
        public int Position;

        //тип карти і її стихія
        public readonly CardType Type;
        public readonly CardElement Element;

        protected Card(string name, int mana, int worth, CardElement element, CardType type) {
            this.Name = name;
            //...
            //стрічки для пошуку в БД опису і піктограмки - це
            //назва карти, обмежена спеціальними символами
            this.Description = String.Format("*{0}*", name);
            PictureName = String.Format("[{0}]", name);

            //початкова позиція карти завжди -1
            Position = -1;
            //...
            ManaCost = mana;
            Element = element;
            GoldWorth = worth;
            Type = type;
            
        }

        //метод жертвування карти
        public virtual void Sacrefice(Player owner, Player enemy) {
            owner.Hand.Remove(this);
            owner.Deck.Add(this);
            owner.Sacreficed = true;

            owner.Call_Sacrefise(this, owner, enemy);
        }
    }
}
