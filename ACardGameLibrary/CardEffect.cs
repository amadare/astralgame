﻿using System.Reflection;

namespace ACardGameEngine {
    //todo: тимчасові еффекти
    public abstract class CardEffect {
        protected CreatureCard Target;
        protected CardEffect(CreatureCard target) {
            Target = target;
        }

        public void Remove() {
            Target.cardEffects.Remove(this);
        }
    }
}