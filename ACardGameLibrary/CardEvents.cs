﻿namespace ACardGameEngine {
    public delegate void CardEventHandler(Card card, Player owner, Player enemy);

    public delegate void PlayerEventHandler(Player owner, Player enemy);

    public delegate void AbilityUseHandler(Ability ability);
}