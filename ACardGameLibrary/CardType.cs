﻿namespace ACardGameEngine {
    public enum CardType {
        Creature, 
        Wall,
        Spell
    }
}