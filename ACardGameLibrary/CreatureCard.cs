﻿using System.Collections.Generic;

namespace ACardGameEngine {
    public class CreatureCard : Card {
        public bool WillAttack;
        private int attack;
        public readonly bool HasAbility;
        public readonly int DefaultAttack,MaxHealth,AbilityCost;
        public int Attack { get { return attack; } set { attack = value < 0 ? 0 : value; } }
        private int health;
        public int Health{get { return health; } set { health = value < MaxHealth ? value : MaxHealth; }}
        public List<CardEffect> cardEffects;
        public CardEventHandler AttackEvent;

        public CreatureCard(string name, int attack, int life, int mana, int worth, CardElement element, CardType type=CardType.Creature,
            bool hasAbility = false, int abilityCost = -1)
            : base(name, mana, worth, element,type) {
            DefaultAttack = attack;
            MaxHealth = life;
            AbilityCost = abilityCost;
            HasAbility = hasAbility;

            Attack = attack;
            Health = MaxHealth;
        }

        public virtual bool Summon(Player owner, Player enemy, int position) {
            if (owner.Mana < ManaCost) return false;
            owner.Mana -= ManaCost;
            owner.Hand.Remove(this);
            owner.Table[position] = this;
            WillAttack = false;
            Health = MaxHealth;
            attack = DefaultAttack;
            Position = position;

            return true;
        }
        public virtual void Hit(Player owner, Player enemy) {
            if (WillAttack) {
                if (AttackEvent!=null)
                    AttackEvent(this, owner, enemy);
                if (enemy.Table[Position]!=null)
                    enemy.Table[Position].TakeDamageFromHit(attack, this, owner, enemy);
                else {
                    enemy.ChangeLife(-attack);
                }
            }
        }

        public virtual void TakeDamageFromHit(int damage, Card dealer, Player dealer_owner, Player reciever_owner) {
            Health -= damage;
            if (Health<=0)
                reciever_owner.CardDied(this, dealer_owner);
        }

        public virtual void ChangeHealthByAbility(int value, Player dealer_owner, Player reciever_owner) {
            Health += value;
            if (Health <= 0)
                reciever_owner.CardDied(this, dealer_owner);
        }

        public virtual void ChangeHealthBySpell(int value, Player dealer_owner, Player reciever_owner) {
            Health += value;
            if (Health <= 0)
                reciever_owner.CardDied(this, dealer_owner);
        }

        public virtual bool CanUseAbility(Player owner, Player enemy) {
            return false;
        }

        public virtual void UseAbility(Player owner, Player enemy, CreatureCard target=null) {}
    }
}