﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace ACardGameEngine {
    public class Player {

        #region Поля і Властивості

        public List<Card> Hand, Deck;
        public CreatureCard[] Table;
        public string Name { get; set; }
        public int Power { get; set; }
        public int Mana { get; set; }
        public int Life { get; set; }
        public Texture2D Avatar;
        private static Random random;
        public bool Sacreficed { get; set; }

        #endregion

        #region Конструктор

        public Player(string name, List<Card> deck, Texture2D avatar) {
            Name = name;
            Deck = deck;
            if (Deck.Count == 30)
                Life = 50;
            else if (Deck.Count <= 40)
                Life = 40;
            else if (Deck.Count <= 50)
                Life = 30;

            Sacreficed = false;
            random = new Random();
            Hand = new List<Card>();
            Table = new CreatureCard[6];
            Avatar = avatar;

        }

        #endregion

        #region Події

        public event CardEventHandler SacrefiseEvent, CastEvent;
        public event PlayerEventHandler TurnBegin;
        public event AbilityUseHandler AbilityUse;
        public event CardEventHandler CardAdded, CardRemoved;

        public void Call_Sacrefise(Card card, Player owner, Player enemy) {
            if (SacrefiseEvent != null)
                SacrefiseEvent(card, owner, enemy);
        }
        public void Call_Cast(Card card, Player owner, Player enemy) {
            if (CastEvent != null)
                CastEvent(card, owner, enemy);
        }

        #endregion

        #region Методи

        public virtual void ChangeLife(int damage) {
            Life += damage;
        }

        public void CastAbility(Ability ability) {
            if (AbilityUse != null)
                AbilityUse(ability);
            ability.Apply();
        }

        public void DrawCard() {
            if (Deck.Count == 0) return;
            int cardIndex = random.Next(0, Deck.Count - 1);
            Hand.Add(Deck[cardIndex]);

            if (CardAdded != null)
                CardAdded(Deck[cardIndex], this, null);

            Deck.RemoveAt(cardIndex);
            }

        public void NewTurnBegin(Player enemy) {
            Sacreficed = false;
            Mana = Power;
            DrawCard();

            foreach (CreatureCard card in Table) {
                if (card!=null)
                    card.WillAttack = true;
            }

            if (TurnBegin != null)
                TurnBegin(this, enemy);
        }

        public void CardDied(CreatureCard card, Player enemy) {
            if (CardRemoved != null)
                CardRemoved(card, this, enemy);
            for (int i = 0; i < Table.Length; i++)
                if (Table[i] == card)
                    Table[i] = null;
            Deck.Add(card);
            card.Attack = card.DefaultAttack;
            card.Health = card.MaxHealth;
        }

        public void AddCard(Card card, Player enemy) {
            Hand.Add(card);
            if (CardAdded != null)
                CardAdded(card, this, enemy);
        }

        public bool Sacrefice(Card card, Player enemy) {
            if (Sacreficed) return false;
            if (Mana == Power) {
                Mana++;
            }
                Power++;
            card.Sacrefice(this,enemy);
            if (CardRemoved != null)
                CardRemoved(card, this, enemy);

            return true;
        }

        #endregion
    }
}