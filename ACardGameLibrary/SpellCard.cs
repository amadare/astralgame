﻿namespace ACardGameEngine {
    public class SpellCard : Card {
        public bool NeedTarget;
        public SpellCard(string name, int mana, int worth, CardElement element, bool needTarget)
            : base(name,mana,worth,element,CardType.Spell) {
            NeedTarget = needTarget;
        }

        public virtual bool Cast(Player owner, Player enemy, CreatureCard target=null) {
            if (owner.Mana < ManaCost) return false;
            owner.Call_Cast(this,owner,enemy);
            owner.Hand.Remove(this);
            owner.Deck.Add(this);
            owner.Mana -= ManaCost;
            return true;
        }
    }
}