﻿namespace ACardGameEngine {
    public abstract class TargetAbility : Ability {
        protected TargetAbility(CreatureCard target, CreatureCard card, Player owner, Player enemy, int cost)
            : base(card,owner,enemy,cost) {
            Target = target;
        }
    }
}