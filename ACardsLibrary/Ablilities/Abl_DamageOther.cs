﻿using ACardGameEngine;

namespace ACardsLibrary.Ablilities {
    public class Abl_DamageOther : TargetAbility {
        public Abl_DamageOther(int value, CreatureCard target, CreatureCard initiator, Player owner, Player enemy, int cost)
            : base(target,initiator,owner,enemy,cost) {
            Type = AbilityType.Target;
            Value = value;
        }

        public override void Apply() {
            Owner.Mana -= Cost;
            Target.ChangeHealthByAbility(-Value,Owner,Enemy);
        }
    }
}