﻿using ACardGameEngine;

namespace ACardsLibrary.Ablilities {
    public class Abl_DamageSelfAndOther : Ability {
        public int SelfDamage;
        public Abl_DamageSelfAndOther(int selfDamage, int otherDamage, CreatureCard card, Player owner, Player enemy, int cost)
            : base(card, owner, enemy, cost) {
            SelfDamage = selfDamage;
            Value = otherDamage;
        }

        public override void Apply() {

        }
    }
}