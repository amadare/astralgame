﻿using ACardGameEngine;

namespace ACardsLibrary.Cards {
    public class Card_Cr_Fire_FireWall : CreatureCard {
        public Card_Cr_Fire_FireWall()
            : base("Card_Cr_Fire_FireWall", 0, 24, 2, 15, CardElement.Fire, CardType.Wall) {
        }

        public override bool Summon(Player owner, Player enemy, int position) {
            if (base.Summon(owner, enemy, position)) {
                foreach (CreatureCard card in enemy.Table) {
                    if (card!=null)
                        card.ChangeHealthBySpell(-4,owner,enemy);
                }
                enemy.ChangeLife(-4);
                return true;
            }
            return false;
        }
    }
}