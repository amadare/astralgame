﻿using ACardGameEngine;
using ACardsLibrary.Ablilities;

namespace ACardsLibrary.Cards {
    public class Card_Cr_Fire_Minotaur : CreatureCard{
        public Card_Cr_Fire_Minotaur()
            : base("Card_Cr_Fire_Minotaur", 5, 20, 5, 10, CardElement.Fire,CardType.Creature) {
        }

        public override bool Summon(Player owner, Player enemy, int position) {

            if (enemy.Table[position]!=null)
                owner.CastAbility(new Abl_DamageOther(8, enemy.Table[position], this, owner, enemy, 0));

            return base.Summon(owner, enemy, position);
        }
    }
}