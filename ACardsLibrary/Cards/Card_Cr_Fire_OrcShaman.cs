﻿using ACardGameEngine;
using ACardsLibrary.Ablilities;

namespace ACardsLibrary.Cards {
    public class Card_Cr_Fire_OrcShaman : CreatureCard {
        public Card_Cr_Fire_OrcShaman()
            : base("Card_Cr_Fire_OrcShaman",15,8,1,20,CardElement.Fire,CardType.Creature,true,4) {
        }

        public override void UseAbility(Player owner, Player enemy, CreatureCard target = null) {
                owner.CastAbility(new Abl_DamageOther(8,target,this,owner,enemy,4));
        }
    }
}