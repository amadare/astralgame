﻿using ACardGameEngine;
namespace ACardsLibrary.Cards {
    public class Card_Cr_Fire_OrcSoldier : CreatureCard {
        public Card_Cr_Fire_OrcSoldier()
            : base("Card_Cr_Fire_OrcSoldier", 4, 14, 2, 5, CardElement.Fire, CardType.Creature) {
        }

        public override bool Summon(Player owner, Player enemy, int position) {
            if (owner.Power<3)
                owner.ChangeLife(-4);

            return base.Summon(owner, enemy, position);
        }
    }
}