﻿using ACardGameEngine;
using ACardsLibrary.Ablilities;

namespace ACardsLibrary.Cards {
    public class Card_Cr_Fire_Salamandra : CreatureCard {
        public Card_Cr_Fire_Salamandra()
            : base("Card_Cr_Fire_Salamandra", 2,36,7,15,CardElement.Fire,CardType.Creature,true,1) {
        }

        public override void Hit(Player owner, Player enemy) {

            if (enemy.Table[Position]==null)
                enemy.ChangeLife(-Attack);
            foreach (CreatureCard card in enemy.Table) {
                if (card!=null)
                    card.TakeDamageFromHit(Attack,this,owner,enemy);
            }



            if (AttackEvent != null)
                AttackEvent(this, owner, enemy);
        }

        public override void UseAbility(Player owner, Player enemy, CreatureCard target = null) {
        }
    }
}