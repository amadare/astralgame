﻿using ACardGameEngine;

namespace ACardsLibrary.Cards {
    public class Card_Sp_AncientHammer : SpellCard {
        public Card_Sp_AncientHammer()
            : base("Card_Sp_AncientHammer", 4, 10, CardElement.Fire, true) {
        }

        public override bool Cast(Player owner, Player enemy, CreatureCard target = null) {
            if (base.Cast(owner, enemy, target)) {
                owner.Call_Cast(this, owner, enemy);
                owner.Hand.Remove(this);
                owner.AddCard(this, enemy);
                owner.Mana -= ManaCost;
                target.ChangeHealthBySpell(-5, owner, enemy);
                return true;
            }
            return false;
        }
    }
}