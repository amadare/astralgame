﻿using ACardGameEngine;

namespace AstralGameContent.CardGame.CardPictures {
    public class Card_Sp_Fire_Big_Fireball : SpellCard {
        public Card_Sp_Fire_Big_Fireball()
            : base("Card_Sp_Fire_Big_Fireball", 5, 10, CardElement.Fire, false) {
        }

        public override bool Cast(Player owner, Player enemy, CreatureCard target = null) {
            if (base.Cast(owner, enemy, target)) {
                foreach (CreatureCard card in enemy.Table) {
                    if (card != null)
                        card.ChangeHealthBySpell(-(3 + owner.Power), owner, enemy);
                }
                return true;
            }
            return false;
        }
    }
}