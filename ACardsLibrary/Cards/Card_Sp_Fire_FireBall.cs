﻿using ACardGameEngine;

namespace ACardsLibrary.Cards {
    public class Card_Sp_Fire_FireBall : SpellCard {
        public Card_Sp_Fire_FireBall()
            : base("Card_Sp_Fire_FireBall", 1, 5, CardElement.Fire,true) {
        }

        public override bool Cast(Player owner, Player enemy, CreatureCard target = null) {
            if (base.Cast(owner, enemy, target)) {
                target.ChangeHealthBySpell(-9, owner, enemy);
                return true;
            }
            return false;
        }
    }
}