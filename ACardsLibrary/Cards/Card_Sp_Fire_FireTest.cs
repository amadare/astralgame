﻿using ACardGameEngine;

namespace ACardsLibrary.Cards {
    public class Card_Sp_Fire_FireTest : SpellCard{
        public Card_Sp_Fire_FireTest()
            : base("Card_Sp_Fire_FireTest", 1, 20, CardElement.Fire, false) {
        }

        public override bool Cast(Player owner, Player enemy, CreatureCard target = null) {
            if (base.Cast(owner, enemy, target)) {
                enemy.ChangeLife(-3);
                foreach (CreatureCard card in enemy.Table) {
                    if (card!=null)
                        card.ChangeHealthBySpell(card.ManaCost,owner,enemy);
                }
                return true;
            }
            return false;
        }
    }
}