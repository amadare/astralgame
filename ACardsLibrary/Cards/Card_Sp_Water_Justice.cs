﻿using ACardGameEngine;

namespace ACardsLibrary.Cards {
    public class Card_Sp_Water_Justice : SpellCard {
        public Card_Sp_Water_Justice() :
            base("Card_Sp_Water_Justice", 3, 5, CardElement.Water, false) {
        }

        public override bool Cast(Player owner, Player enemy, CreatureCard target = null) {
            if (base.Cast(owner, enemy, target)) {
                foreach (CreatureCard card in enemy.Table) {
                    if (card != null) {
                        card.ChangeHealthBySpell(-card.Attack,owner,enemy);
                    }
                }
                owner.DrawCard();
                return true;
            }
            return false;
        }
    }
}