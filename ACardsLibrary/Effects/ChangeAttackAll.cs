﻿using ACardGameEngine;

namespace ACardsLibrary.Effects {
    public class ChangeAttackAll : CardEffect {
        private int value;
        public ChangeAttackAll(int value,CreatureCard target, Player owner, Player enemy):base(target){
            this.value = value;
            enemy.CastEvent += EnemyCast;

            foreach (var card in enemy.Table)
                if (card != null)
                    card.Attack += value;
        }

        private void EnemyCast(Card card,Player owner, Player enemy) {
            if (card is CreatureCard) {
                ((CreatureCard) card).Attack += value;
            }
        }
    }
}