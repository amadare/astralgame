﻿using ACardGameEngine;

namespace ACardsLibrary.Effects {
    public class IncreaseAttackWhileEmptySlot:CardEffect {
        private readonly int incValue;
        private readonly CreatureCard target;
        public IncreaseAttackWhileEmptySlot(int incValue,CreatureCard target, Player owner, Player enemy) 
            : base(target) {
            this.incValue = incValue;
            this.target = target;

            Update(owner,enemy);
            owner.TurnBegin += Update;
        }

        public void Update(Player owner, Player enemy) {
            if (enemy.Table[target.Position] == null)
                target.Attack += incValue;
        }
    }
}