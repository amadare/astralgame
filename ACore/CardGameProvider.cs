﻿using System.Diagnostics;
using ACardGameEngine;
using Microsoft.Xna.Framework;

namespace ACore {
    public delegate void CardDrawEventHandler(Card card, bool currentPlayer);
    public delegate void NextTurnEventHandler();

    public class CardGameProvider {
        public Player Player1, Player2;
        public bool Player1Turn, Atatking;
        public static int AttackDelay = 800;
        private int timeElapsed, currentAttackingCard;
        public Player CurrentPlayer { get { return Player1Turn ? Player1 : Player2; } }
        public Player EnemyPlayer { get { return Player1Turn ? Player2 : Player1; } }

        //public event CardDrawEventHandler CardDrawEvent, CardRemoveEvent;

        public CardGameProvider(Player player1, Player player2) {
            Player1 = player1;
            Player2 = player2;
            Player1Turn = true;
            Atatking = false;
            timeElapsed = 0;
            currentAttackingCard = 0;
        }

        public void NextTurn() {
            if (Atatking) return;
            Atatking = true;
        }
        private void nextTurn() {
            Player1Turn = !Player1Turn;
            CurrentPlayer.NewTurnBegin(EnemyPlayer);

            Debug.WriteLine(string.Format("{0}:{1}; {2}:{3}. Current:{4}", CurrentPlayer.Name,
                CurrentPlayer.Hand.Count.ToString(),
                EnemyPlayer.Name, EnemyPlayer.Hand.Count.ToString(),
                CurrentPlayer.Name));
        }

        public void CastSpell(SpellCard card, CreatureCard target = null) {
            if (CurrentPlayer.Mana>=card.ManaCost)
                card.Cast(CurrentPlayer, EnemyPlayer, target);
        }

        public bool SummonCreature(CreatureCard card, int position) {
            if (CurrentPlayer.Table[position] != null) return false;
            return card.Summon(CurrentPlayer, EnemyPlayer, position);
        }

        public void Update(GameTime gameTime) {
            if (Atatking) {
                timeElapsed += gameTime.ElapsedGameTime.Milliseconds;
                if (CurrentPlayer.Table[currentAttackingCard] == null) //існування
                    currentAttackingCard++;
                else
                    if (!CurrentPlayer.Table[currentAttackingCard].WillAttack) //можливість
                        currentAttackingCard++;
                    else if (timeElapsed >= AttackDelay) { //час
                        timeElapsed -= AttackDelay;
                        CurrentPlayer.Table[currentAttackingCard].Hit(CurrentPlayer, EnemyPlayer);
                        currentAttackingCard++;
                    }

                if (currentAttackingCard == 6) {
                    currentAttackingCard = 0;
                    Atatking = false;
                    timeElapsed = 0;
                    nextTurn();
                }
            }
        }

        public void UseAbility(CreatureCard card, CreatureCard target = null) {
            card.UseAbility(CurrentPlayer, EnemyPlayer, target);
        }

        public void Sacrefice(Card card) {
            CurrentPlayer.Sacrefice(card, EnemyPlayer);
        }
    }
}