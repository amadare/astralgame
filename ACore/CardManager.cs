﻿using System;
using System.Reflection;
using System.Linq;
using ACardGameEngine;

namespace ACore {
    public class CardManager {
        private string path;
        private Assembly cardLib;
        private Type[] cardTypes;

        public CardManager(string path) {
            cardLib = Assembly.LoadFrom(path);

            cardTypes = cardLib.GetTypes().Where(type => (type.IsSubclassOf(typeof(Card)))).ToArray();
        }

        public Card[] GetAllCards() {
            Card[] allCards = new Card[cardTypes.Length];

            for (int i = 0; i < cardTypes.Length; i++) {
                allCards[i] = cardTypes[i].Assembly.CreateInstance(cardTypes[i].FullName) as Card;
            }

            return allCards;
        }

        public Card GetCardByName(string name) {
            foreach (Type type in cardTypes) {
                if (type.Name == name)
                    return ((Card)type.Assembly.CreateInstance(name));
            }
            return null;
        }

        public Card GetCardByFullName(string fullName) {
            foreach (Type type in cardTypes) {
                if (type.FullName == fullName)
                    return ((Card)type.Assembly.CreateInstance(fullName));
            }
            return null;
        }

    }
}