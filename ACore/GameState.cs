using System;
using System.Collections.Generic;
using AInputLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using AInterfaceLibrary;

namespace AInterfaceLibrary {
    public abstract class GameState : DrawableGameComponent {

        #region ���� � ����������

        //������ �� �������� ������
        //�������� ���� ��� �������� �����
        protected GameStateManager StateManager;

        //��������� ��������� ��������. 
        //��� ������� �������� ����� ���
        public ControlManager controlManager;

        protected SpriteBatch spriteBatch;

        #endregion

        #region �����������

        protected GameState(Game game, GameStateManager manager, SpriteBatch spriteBatch)
            : base(game) {
            StateManager = manager;
            controlManager = new ControlManager();
            this.spriteBatch = spriteBatch;
        }

        #endregion

        #region ��������� �����

        public virtual void DrawGui(SpriteBatch spriteBatch) { }
        public virtual void DrawAfterGui(SpriteBatch spriteBatch) { }
        public virtual void DrawBeforeGui(SpriteBatch spriteBatch) { }
        public virtual void DrawCursor(SpriteBatch spriteBatch) { }

        #endregion

        #region ������ � ������

        //������ Show i Hide ����������
        //������/���������� �� ���������� �����
        public void Show() {
            this.Visible = true;
            this.Enabled = true;
        }
        public void Hide() {
            this.Visible = false;
            this.Enabled = false;
        }

        //StateChange ��������� �� ���� ���� ��������� �����
        //�����/������ ���� ��� �����������
        internal protected virtual void StateChange(object sender, EventArgs e) {
            if (GameStateManager.CurrentState == this)
                Show();
            else
                Hide();
        }

        #endregion

        #region ������ � GUI

        //������ �������� �������� ������������ ������
        protected List<Control> lastControlsOnCursor;

        //�������� ���. ���� ����� �� ����

        public override void Update(GameTime gameTime) {
            
            //�������� �� �������� � ��������� �����
            List<Control> currentControlsOnCursor = controlManager.GetControls(Cursor.CursorPosition);

            //�� � ���� �������� �� ����� �����
            if (currentControlsOnCursor != null)
                #region ����������

                for (int key = 0; key < 5; key++) {
                    //����������
                    if (InputHandler.MouseKeyPressed(key))
                        currentControlsOnCursor.ForEach(
                        x => {
                            if (x.AllowClick)
                                x.CallEvent_MousePress(key);
                        });

                    //����������
                    if (InputHandler.MouseKeyReleased(key))
                        currentControlsOnCursor.ForEach(x => {
                            if (x.AllowClick)
                                x.CallEvent_MouseRelease(key);
                        });
                }

                #endregion

            #region ���������

            //MouseHoverIn
            if (currentControlsOnCursor != null)
                if (lastControlsOnCursor == null)
                    foreach (Control control in currentControlsOnCursor)
                        control.CallEvent_MouseHoverIn(Cursor.CursorPosition);
                else
                    foreach (Control control in currentControlsOnCursor) {
                        if (!lastControlsOnCursor.Contains(control))
                            control.CallEvent_MouseHoverIn(Cursor.CursorPosition);
                    }

            //MouseHoverOut
            if (lastControlsOnCursor != null)
                if (currentControlsOnCursor == null)
                    foreach (Control control in lastControlsOnCursor) {
                        control.CallEvent_MouseHoverOut(Cursor.CursorPosition);
                    }
                else {
                    foreach (Control control in lastControlsOnCursor) {
                        if (!currentControlsOnCursor.Contains(control))
                            control.CallEvent_MouseHoverOut(Cursor.CursorPosition);
                    }
                }

            #endregion

            //���������� �������� �������
            lastControlsOnCursor = currentControlsOnCursor;
        }

        public override void Draw(GameTime gameTime) {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            controlManager.Draw(spriteBatch);
            DrawCursor(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        #endregion

    }
}
