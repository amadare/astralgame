using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace AInterfaceLibrary
{
    /*����� ����� ��������� � ������ �����. 
     * ����, �� ������� - �� �������� ����. ��� ���� �������,
     * ���������� ����� ���� ����.
     */
    public class GameStateManager : GameComponent
    {
        #region ��䳿
        //���� ���� �����, �� ��� �������� �� �����
        public event EventHandler OnStateChange;
        #endregion

        #region ���� � ����������

        //���� ������� ������
        static Stack<GameState> gameStates = new Stack<GameState>();

        //�������� ����-��� ������� �������� ��������
        //�������� ������� ����
        static public GameState CurrentState
        {
            get { return gameStates.Peek(); }
        }

        //drawOrder ���������� ��� ������������� 
        //�������������� ������.
        const int startDrawOrder = 5000;
        const int drawOrderInc = 100;
        int drawOrder;

        #endregion

        #region �����������

        public GameStateManager(Game game)
            : base(game)
        {
            drawOrder = startDrawOrder;
        }

        #endregion

        #region ������

        //AddState ���� ���� �� ���������� ��� � ������ 
        //�� ���� ���� �����. ��������� ���� � ��������.
        private void AddState(GameState newState)
        {
            gameStates.Push(newState);
            Game.Components.Add(newState);
            OnStateChange += newState.StateChange;

            Debug.WriteLine(newState.ToString() + " added.");
        }

        //RemoveState ������� ���� �� ���������� ��� � ������ 
        //�� ��䳿 ���� �����. ��������� ���� � ��������.
        private void RemoveState()
        {
            GameState State = gameStates.Peek();
            OnStateChange -= State.StateChange;
            Game.Components.Remove(State);
            gameStates.Pop();
            Debug.WriteLine(State.ToString() + " removed.");
        }

        //PushState ���� ���� �� �����. ³� ��� ��� 
        //drawOrder � ������� ���� ���� �����
        public void PushState(GameState newState)
        {
            drawOrder += drawOrderInc;
            newState.DrawOrder = drawOrder;

            AddState(newState);

            if (OnStateChange != null)
                OnStateChange(this, null);

            
        }

        //PopState ������� ���� � ������� �����. ³� ��� ��� 
        //drawOrder � ������� ���� ���� �����
        public void PopState()
        {
            if (gameStates.Count > 0)
            {
                Debug.WriteLine(CurrentState.ToString() + " poped.");
                RemoveState();

                //CurrentState.Show();
                
                drawOrder -= drawOrderInc;

                if (OnStateChange != null)
                    OnStateChange(this, null);
            }
        }

        //ChangeState ����� ���� ����, �������� ���� �����
        //������. ������� drawOrder � ������� ���� ���� �����.
        public void ChangeState(GameState newState)
        {
            while (gameStates.Count>0)
                RemoveState();

            newState.DrawOrder = startDrawOrder;
            drawOrder = startDrawOrder;

            AddState(newState);

            if (OnStateChange != null)
                OnStateChange(this, null);
        }

        #endregion
    }
}
