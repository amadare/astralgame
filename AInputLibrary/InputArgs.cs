﻿namespace AInputLibrary {
    public enum InputDevice {
        Gamepad,Keyboard,Mouse
    }
    public class InputArgs {
        public readonly InputDevice Device;
        public readonly object Key;
        public readonly bool State;

        public InputArgs(InputDevice device, object key, bool state) {
            State = state;
            Key = key;
            Device = device;
        }
    }
}