using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Linq;

namespace AInputLibrary {
    public class InputHandler {

        //������� ��� ����������� ������� �� ������
        [DllImport("user32.dll")]
        static extern void ClipCursor(ref Rectangle rect);

        #region ��䳿

        static public event MouseMoveEventHandler MouseMove;
        public static event MouseButtonEvent MouseButtonReleased, MouseButtonPressed;


        #endregion

        #region ����

        //keyboardState �������� ���� ���������
        //lastKeyboardState ��� ���� ������ ��� ������������ �����

        private static KeyboardState keyboardState;
        private static KeyboardState lastKeyboardState;

        private static MouseState mouseState;
        private static MouseState lastMouseState;

        public static Dictionary<InputArgs, int> InputCodes;
        public static Dictionary<int, MouseKey> MouseKeyBindings;

        private Game game;

        static public bool HoldMouseInWindow { get; set; }

        #endregion

        #region ����������
        //����� ��������� � ����
        public static KeyboardState KeyboardState {
            get { return keyboardState; }
        }
        public static KeyboardState LastKeyboardState {
            get { return lastKeyboardState; }
        }

        public static MouseState MouseState {
            get { return mouseState; }
        }
        public static MouseState LastMouseState {
            get { return lastMouseState; }
        }

        public static Vector2 MousePosition {
            get {
                return new Vector2(mouseState.X, mouseState.Y);
            }
        }

        //������� ������ ���� �������� ����
        public static int DeltaMouseWheel {
            get { return mouseState.ScrollWheelValue - lastMouseState.ScrollWheelValue; }
        }

        #endregion

        #region �����������

        public InputHandler(Game game) {
            //�������� ������� ����� �����������
            keyboardState = Keyboard.GetState();
            mouseState = Mouse.GetState();

            Mouse.WindowHandle = game.Window.Handle;
            HoldMouseInWindow = false;
            InputCodes = new Dictionary<InputArgs, int>();

            MouseKeyBindings = new Dictionary<int, MouseKey>();

            this.game = game;

            int i = 0;
            foreach (MouseKey key in Enum.GetValues(typeof(MouseKey))) {
                MouseKeyBindings.Add(i, key);
                i++;
            }
        }

        #endregion

        #region ������ XNA

        //� ������ �������� Update, ����� ���������
        //��������� ���������� �������
        public void Update(GameTime gameTime) {
            if (game.IsActive) {
                //�����
                lastKeyboardState = keyboardState;
                keyboardState = Keyboard.GetState();

                //����
                lastMouseState = mouseState;
                mouseState = Mouse.GetState();

                if (MouseMove != null)
                    if (lastMouseState.X != mouseState.X || lastMouseState.Y != mouseState.Y)
                        MouseMove(new Vector2(mouseState.X, mouseState.Y), new Vector2(lastMouseState.X, lastMouseState.Y));

                for (int i = 0; i < 5; i++)
                    if (MouseKeyReleased(i) && MouseButtonReleased != null)
                        MouseButtonReleased(i);
                    else if (MouseKeyPressed(i) && MouseButtonPressed != null)
                        MouseButtonPressed(i);


                //����������� �������
                if (HoldMouseInWindow) {
                    Rectangle rect = game.Window.ClientBounds;
                    rect.Width += rect.X;
                    rect.Height += rect.Y;

                    ClipCursor(ref rect);
                }
            }
        }
        #endregion

        #region ���������

        public static bool KeyPressed(Keys key) {
            return keyboardState.IsKeyDown(key) &&
                    !LastKeyboardState.IsKeyDown(key);
        }
        public static bool KeyReleased(Keys key) {
            return !KeyboardState.IsKeyDown(key) &&
             LastKeyboardState.IsKeyDown(key);
        }
        public static bool KeyDown(Keys key) {
            return keyboardState.IsKeyDown(key);
        }
        public void Flush() {
            lastKeyboardState = KeyboardState;
        }


        static public List<Keys> KeyboardAllPressedKeys() {
            return new List<Keys>(keyboardState.GetPressedKeys());
        }

        #endregion

        #region ����

        //������� ���� ���������� ����� �� �����
        static public bool MouseKeyPressed(MouseKey key) {
            switch (key) {
                #region ������
                case MouseKey.Left:
                    return ((lastMouseState.LeftButton == ButtonState.Released) &&
                        (mouseState.LeftButton == ButtonState.Pressed));

                case MouseKey.Right:
                    return ((lastMouseState.RightButton == ButtonState.Released) &&
                        (mouseState.RightButton == ButtonState.Pressed));

                case MouseKey.Middle:
                    return ((lastMouseState.MiddleButton == ButtonState.Released) &&
                        (mouseState.MiddleButton == ButtonState.Pressed));

                case MouseKey.XButton1:
                    return ((lastMouseState.XButton1 == ButtonState.Released) &&
                        (mouseState.XButton1 == ButtonState.Pressed));

                case MouseKey.XButton2:
                    return ((lastMouseState.XButton2 == ButtonState.Released) &&
                        (mouseState.XButton2 == ButtonState.Pressed));

                default:
                    return true;
                #endregion
            }
        }
        static public bool MouseKeyReleased(MouseKey key) {
            switch (key) {
                #region ������
                case MouseKey.Left:
                    return ((lastMouseState.LeftButton == ButtonState.Pressed) &&
                        (mouseState.LeftButton == ButtonState.Released));

                case MouseKey.Right:
                    return ((lastMouseState.RightButton == ButtonState.Pressed) &&
                        (mouseState.RightButton == ButtonState.Released));

                case MouseKey.Middle:
                    return ((lastMouseState.MiddleButton == ButtonState.Pressed) &&
                        (mouseState.MiddleButton == ButtonState.Released));

                case MouseKey.XButton1:
                    return ((lastMouseState.XButton1 == ButtonState.Pressed) &&
                        (mouseState.XButton1 == ButtonState.Released));

                case MouseKey.XButton2:
                    return ((lastMouseState.XButton2 == ButtonState.Pressed) &&
                        (mouseState.XButton2 == ButtonState.Released));

                default:
                    return true;
                #endregion
            }
        }
        static public bool MouseKeyDown(MouseKey key) {
            switch (key) {
                #region ������
                case MouseKey.Left:
                    return mouseState.LeftButton == ButtonState.Pressed;

                case MouseKey.Right:
                    return mouseState.RightButton == ButtonState.Pressed;

                case MouseKey.Middle:
                    return mouseState.MiddleButton == ButtonState.Pressed;

                case MouseKey.XButton1:
                    return mouseState.XButton1 == ButtonState.Released;

                case MouseKey.XButton2:
                    return mouseState.XButton2 == ButtonState.Released;

                default:
                    return true;
                #endregion
            }
        }

        //������� ���� ���������� ����� �� �������
        static public bool MouseKeyDown(int key) {
            switch (MouseKeyBindings[key]) {
                #region ������
                case MouseKey.Left:
                    return mouseState.LeftButton == ButtonState.Pressed;

                case MouseKey.Right:
                    return mouseState.RightButton == ButtonState.Pressed;

                case MouseKey.Middle:
                    return mouseState.MiddleButton == ButtonState.Pressed;

                case MouseKey.XButton1:
                    return mouseState.XButton1 == ButtonState.Released;

                case MouseKey.XButton2:
                    return mouseState.XButton2 == ButtonState.Released;

                default:
                    return true;
                #endregion
            }
        }
        static public bool MouseKeyPressed(int key) {
            switch (MouseKeyBindings[key]) {
                #region ������
                case MouseKey.Left:
                    return ((lastMouseState.LeftButton == ButtonState.Released) &&
                        (mouseState.LeftButton == ButtonState.Pressed));

                case MouseKey.Right:
                    return ((lastMouseState.RightButton == ButtonState.Released) &&
                        (mouseState.RightButton == ButtonState.Pressed));

                case MouseKey.Middle:
                    return ((lastMouseState.MiddleButton == ButtonState.Released) &&
                        (mouseState.MiddleButton == ButtonState.Pressed));

                case MouseKey.XButton1:
                    return ((lastMouseState.XButton1 == ButtonState.Released) &&
                        (mouseState.XButton1 == ButtonState.Pressed));

                case MouseKey.XButton2:
                    return ((lastMouseState.XButton2 == ButtonState.Released) &&
                        (mouseState.XButton2 == ButtonState.Pressed));

                default:
                    return true;
                #endregion
            }
        }
        static public bool MouseKeyReleased(int key) {
            switch (MouseKeyBindings[key]) {
                #region ������
                case MouseKey.Left:
                    return ((lastMouseState.LeftButton == ButtonState.Pressed) &&
                        (mouseState.LeftButton == ButtonState.Released));

                case MouseKey.Right:
                    return ((lastMouseState.RightButton == ButtonState.Pressed) &&
                        (mouseState.RightButton == ButtonState.Released));

                case MouseKey.Middle:
                    return ((lastMouseState.MiddleButton == ButtonState.Pressed) &&
                        (mouseState.MiddleButton == ButtonState.Released));

                case MouseKey.XButton1:
                    return ((lastMouseState.XButton1 == ButtonState.Pressed) &&
                        (mouseState.XButton1 == ButtonState.Released));

                case MouseKey.XButton2:
                    return ((lastMouseState.XButton2 == ButtonState.Pressed) &&
                        (mouseState.XButton2 == ButtonState.Released));

                default:
                    return true;
                #endregion
            }
        }

        static public List<MouseKey> MouseAllPressedKeys() {
            List<MouseKey> keys = new List<MouseKey>();
            if ((lastMouseState.LeftButton == ButtonState.Pressed) &&
                (mouseState.LeftButton == ButtonState.Released)) keys.Add(MouseKey.Left);

            if ((lastMouseState.RightButton == ButtonState.Pressed) &&
                    (mouseState.RightButton == ButtonState.Released)) keys.Add(MouseKey.Right);

            if ((lastMouseState.MiddleButton == ButtonState.Pressed) &&
                    (mouseState.MiddleButton == ButtonState.Released)) keys.Add(MouseKey.Middle);

            if ((lastMouseState.XButton1 == ButtonState.Pressed) &&
                    (mouseState.XButton1 == ButtonState.Released)) keys.Add(MouseKey.XButton1);

            if ((lastMouseState.XButton2 == ButtonState.Pressed) &&
                    (mouseState.XButton2 == ButtonState.Released)) keys.Add(MouseKey.XButton2);
            return keys;
        }

        static public List<MouseKey> MouseGetStatePressedButtons(MouseState state) {
            List<MouseKey> buttons = new List<MouseKey>();

            if (state.LeftButton == ButtonState.Pressed)
                buttons.Add(MouseKey.Left);
            if (state.RightButton == ButtonState.Pressed)
                buttons.Add(MouseKey.Right);
            if (state.MiddleButton == ButtonState.Pressed)
                buttons.Add(MouseKey.Middle);
            if (state.XButton1 == ButtonState.Pressed)
                buttons.Add(MouseKey.XButton1);
            if (state.XButton2 == ButtonState.Pressed)
                buttons.Add(MouseKey.XButton2);

            return buttons;
        }

        //����� ����� ����
        static public void MouseFlush() {
            mouseState = lastMouseState;
        }
        #endregion


        #region ToDo: ���������������� �-���

        public static List<int> GetInputList() {
            List<InputArgs> args = new List<InputArgs>();
            List<int> result = new List<int>();

            List<Keys> keys = new List<Keys>(keyboardState.GetPressedKeys());
            List<Keys> lastKeys = new List<Keys>(lastKeyboardState.GetPressedKeys());

            if (keys != lastKeys && keys.Count > 0) { }

            foreach (Keys key in keys) {
                if (!lastKeys.Contains(key))
                    args.Add(new InputArgs(InputDevice.Keyboard, key, true));
            }

            foreach (Keys key in lastKeys) {
                if (!keys.Contains(key))
                    args.Add(new InputArgs(InputDevice.Keyboard, key, false));
            }

            #region Mouse
            List<MouseKey> lastButtons = MouseGetStatePressedButtons(lastMouseState);
            List<MouseKey> buttons = MouseGetStatePressedButtons(mouseState);

            foreach (MouseKey key in buttons) {
                if (!lastButtons.Contains(key))
                    args.Add(new InputArgs(InputDevice.Mouse, key, true));
            }

            foreach (MouseKey key in lastButtons) {
                if (!buttons.Contains(key))
                    args.Add(new InputArgs(InputDevice.Mouse, key, false));
            }
            #endregion

            foreach (InputArgs arg in args) {
                var code = InputCodes.FirstOrDefault(x => x.Key.Device == arg.Device && x.Key.Key == arg.Key && x.Key.State == arg.State);
                //result.Add(code);
            }

            if (result.Count > 0) {

            }

            return result;
        }

        #region �������
        public static bool GamepadButtonDown(int num, int key) {
            return false;//todo gamepads
        }
        #endregion

        #endregion
    }
}
