﻿namespace AInputLibrary
{
    public enum MouseKey
    {
        Left,
        Right,
        Middle,
        XButton1,
        XButton2
    }
}