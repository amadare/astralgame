﻿using Microsoft.Xna.Framework;

namespace AInputLibrary {
    public delegate void MouseMoveEventHandler(Vector2 currentPosition, Vector2 lastPosition);

    public delegate void MouseButtonEvent(int button);
}