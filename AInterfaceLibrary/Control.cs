﻿using System.Collections.Generic;
using AInterfaceLibrary.Controls.Effects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary {
    public abstract class Control {

        #region Події Миші

        public delegate void MousePressEventHandler(int key);

        public delegate void MouseHoverEvent(Vector2 cursorPosition);

        public event MouseHoverEvent MouseHoverIn;
        public event MouseHoverEvent MouseHoverOut;
        public event MousePressEventHandler MousePress;
        public event MousePressEventHandler MouseRelease;

        public virtual void CallEvent_MouseHoverIn(Vector2 cursorPosition) {
            if (MouseHoverIn!=null && AllowHover)
                MouseHoverIn(cursorPosition);
        }
        public virtual void CallEvent_MouseHoverOut(Vector2 cursorPosition) {
            if (MouseHoverOut != null && AllowHover)
                MouseHoverOut(cursorPosition);
        }
        public virtual void CallEvent_MousePress(int key) {
            if (MousePress != null && AllowClick) {
                MousePress(key);
            }
        }
        public virtual void CallEvent_MouseRelease(int key) {
            if (MouseRelease != null && AllowClick) {
                MouseRelease(key);
            }
        }

        #endregion

        #region Позиціонування

        //чи "вести" за собою контроли при переміщенні
        protected bool leadChildrens;

        //абсолютна позиція і розміри
        protected Vector2 position;
        protected Vector2 size;
        public Vector2 PositionVector {
            get { return position; }
            set {
                if (leadChildrens)
                    foreach (Control control in ChildControls)
                        control.Shift(value.X - position.X, value.Y-position.Y);
                position = value;

            }
        }
        public Vector2 SizeVector {
            get { return new Vector2(size.X*scale,size.Y*scale); }
            set { size = value; }
        }

        public Vector2 Origin;
        public void SetOriginToCenter() {
            Origin.X = size.X/2;
            Origin.Y = size.Y/2;
        }

        public float SizeX { get { return this.size.X * this.scale; } set { size.X = value; } }
        public float SizeY { get { return this.size.Y * this.scale; } set { size.Y = value; } }

        //прямокутник контрола
        public Rectangle ControlRectangle {
            get {
                return new Rectangle((int)position.X, (int)position.Y,
                    (int)(size.X*scale), (int)(size.Y*scale));
            }
        }

        //зміщення контролу разом з дочірніми контролами
        public void Shift(float x, float y) {
            position.X += x;
            position.Y += y;

            if (leadChildrens)
                foreach (Control control in childControls) {
                    control.Shift(x, y);
                }
        }

        //Позиція відносно батьківського контрола
        public Vector2 RelativePosition {
            get {
                if (parentControl == null)
                    return position;
                else {
                    return new Vector2 {
                        X = position.X - parentControl.PositionVector.X,
                        Y = position.Y - parentControl.PositionVector.Y
                    };
                }
            }
            set {
                position.X = value.X + parentControl.PositionVector.X;
                position.Y = value.Y + parentControl.PositionVector.Y;
            }
        }

        //встановлюємо/отримуємо абсолютну позицію
        public float PositionX {
            get { return position.X; }
            set {
                //зміщення дочірніх контролів
                if (this.leadChildrens) {
                    float delta = value - position.X;
                    foreach (Control child in childControls)
                        child.Shift(delta, 0);
                }

                //зміщення основного контрола
                position.X = value;
            }
        }
        public float PositionY {
            get { return position.Y; }
            set {
                //зміщення дочірніх контролів
                float delta = (int)value - position.Y;
                if (this.leadChildrens)
                    foreach (Control child in childControls)
                        child.Shift(0, delta);

                //зміщення основного контрола
                position.Y += delta;
            }
        }

        #endregion

        #region Шари

        protected float layerDepth;

        public float LayerDepth {
            get {
                return layerDepth > 1 ? 1 : layerDepth;
            }
            set { layerDepth = value; }
        }
        public float TrueLayerDepth { get { return layerDepth; } }

        #endregion

        #region Наслідування

        public virtual void AddChild(Control control) {
            control.parentControl = this;
            childControls.Add(control);
        }

        public virtual void RemoveChild(Control child) {
            childControls.Remove(child);
        }

        public void AssignParrent(Control newParent) {
            parentControl = newParent;
            newParent.childControls.Add(this);
        }

        protected Control parentControl;
        protected List<Control> childControls;

        public Control ParrentControl {
            get { return parentControl; }
        }

        public List<Control> ChildControls {
            get { return childControls; }
        } 

        #endregion

        #region Поля

        protected List<ControlEffect> controlEffects;

        protected SpriteFont spriteFont;
        protected Color color;
        protected ControlManager controlManager;
        protected string type;
        protected bool visible;
        public bool AllowHover,         //дозволяє викликати події Mouse_In i Mouse_Hover
                    CursorTargetable,   //дозволяє ControlManager.GetControls отримувати цей контрол 
                                        //з метода this.Intersects
                    AllowClick;         //дозволяє викликати події Mouse_Press i Mouse_Release
        //TODO: experimental/not realized
        public bool GetInput { get; set; }

        public SpriteEffects ControlSpriteEffects;

        protected float opacity, rotation, scale;

        #endregion

        #region Властивості

        public SpriteFont SpriteFont {
            get { return spriteFont; }
            set { spriteFont = value; }
        }
        public Color ControlColor {
            get { return color*opacity; }
            set { color = value; }
        }
        public string Type {
            get { return type; }
            set { type = value; }
        }
        public bool Visible {
            get { return visible; }
            set { visible = value; }
        }
        public float Opacity {
            get { return opacity; }
            set {
                opacity = value;
            }
        }
        public List<ControlEffect> ControlEffects {
            get { return controlEffects; }
            set { controlEffects = value; }
        }
        public override string ToString() {
            return Type;
        }

        public float Rotation {
            get { return rotation; }
            set { rotation = value; }
        }

        public float Scale {
            get { return scale; }
            set { scale = value; }
        }

        #endregion

        #region Конструктор

        protected Control(Game game,ControlManager manager) {

            #region дефолтні налаштування

            ControlColor = Color.White;
            Visible = true;
            SpriteFont = manager.SpriteFont;
            leadChildrens = true;
            AllowHover = true;
            AllowClick = true;
            CursorTargetable = true;
            opacity = 1f;

            ControlEffects = new List<ControlEffect>();
            childControls = new List<Control>();

            Type = this.ToString();

            #endregion

            parentControl = null;
            LayerDepth = manager.DefaultLayerDepth;

            rotation = 0;
            scale = 1;
            Origin = Vector2.Zero;

            manager.Controls.Add(this);

            controlManager = manager;
        }

        #endregion

        #region Віртуальні/абстрактні Методи

        public abstract void Draw(SpriteBatch spriteBatch);

        public virtual void Update(GameTime gameTime) {
            for (int i = 0; i < controlEffects.Count; i++) {
                if (controlEffects[i].ToDelete) {
                    controlEffects.RemoveAt(i);
                    i--;
                }
            }
        }

        public virtual void HandleInput() {}

        #endregion

        #region Методи

        public void Remove() {
            controlManager.Controls.Remove(this);
        }

        //перевіряємо, чи вектор знаходиться на контролі
        public bool Intersects(Vector2 point) {
            return (point.X >= position.X && point.X <= position.X + SizeX
                && point.Y >= position.Y && point.Y <= position.Y + SizeY);
        }

        public void CenterOnParrent(bool X=true,bool Y=true, int dX=0,int dY=0) {
            float x = (parentControl.SizeX - this.SizeX) / 2+dX;
            float y = (parentControl.SizeY - this.SizeY) / 2+dY;

            this.RelativePosition = new Vector2(X ? x : RelativePosition.X, Y ? (int)y : RelativePosition.Y);
        }

        #endregion
    }
}