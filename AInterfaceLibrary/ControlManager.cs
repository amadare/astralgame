﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary {
    public enum ControlEventSendType {
        OnlyTopmost, All
    }
    public class ControlManager {
        public SpriteFont SpriteFont;
        public static SpriteFont DefaultSpriteFont;
        public ControlEventSendType ControlEventSendType;
        public List<Control> Controls;
        public float DefaultLayerDepth;

        public ControlManager(SpriteFont font=null) {
            Controls = new List<Control>();
            ControlEventSendType = ControlEventSendType.All;
            DefaultLayerDepth = .1f;
            SpriteFont = font??DefaultSpriteFont;
        }

        public List<Control> GetControls(Vector2 position) {
            List<Control> controlsOnCursor = Controls.Where(control => control.CursorTargetable && control.Intersects(position)).ToList();

            if (ControlEventSendType == ControlEventSendType.OnlyTopmost) {
                float maxDepth = controlsOnCursor.Select(t => t.LayerDepth).Concat(new float[] { 0 }).Max();
                return controlsOnCursor.Where(control => !(control.LayerDepth < maxDepth)).ToList();
            }
            return controlsOnCursor;
        }

        public void Draw(SpriteBatch spriteBatch) {
            foreach (Control control in Controls) {
                control.Draw(spriteBatch);
            }
        }

        public void Update(GameTime gameTime) {
            foreach (Control control in Controls) {
                control.Update(gameTime);
            }
        }
    }
}