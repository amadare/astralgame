﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary.Controls {
    enum ButtonState {
        Normal, Pressed, Hovered
    }

    public enum HightlightType {
        ChangePicture, Hightlight
    }

    public class Button : Control {
        private Texture2D normalTx;
        private Texture2D hoverTx;
        private Texture2D pressedTx;
        private ButtonState buttonState;
        public List<int> KeyTriggers;

        public HightlightType hightlightType = HightlightType.ChangePicture;
        public Vector2 HightlightPosition = Vector2.Zero;

        #region Конструктори

        public Button(Game game, ControlManager manager, Vector2 position, Vector2 size, Texture2D normalTx, Texture2D hoverTx = null, Texture2D pressedTx = null)
            : base(game, manager) {
            this.position = position;
            this.size = size;

            this.NormalTx = normalTx;
            this.HoverTx = hoverTx ?? normalTx;
            this.PressedTx = pressedTx ?? normalTx;

            KeyTriggers = new List<int>();
            KeyTriggers.Add(0);
        }
        public Button(Game game, ControlManager manager, float positionX, float positionY, float SizeX, float SizeY, Texture2D normalTx, Texture2D hoverTx = null, Texture2D pressedTx = null)
            : base(game, manager) {
            position = new Vector2(positionX, positionY);
            size = new Vector2(SizeX, SizeY);

            this.NormalTx = normalTx;
            this.HoverTx = hoverTx ?? normalTx;
            this.PressedTx = pressedTx ?? normalTx;

            KeyTriggers = new List<int>();
            KeyTriggers.Add(0);
        }
        public Button(Game game, ControlManager manager, float positionX, float positionY, Texture2D normalTx, Texture2D hoverTx = null, Texture2D pressedTx = null)
            : base(game, manager) {
            position = new Vector2(positionX, positionY);
            size = new Vector2(normalTx.Width, normalTx.Height);

            this.NormalTx = normalTx;
            this.HoverTx = hoverTx ?? normalTx;
            this.PressedTx = pressedTx ?? normalTx;

            KeyTriggers = new List<int>();
            KeyTriggers.Add(0);
        }

        #endregion

        #region Властивості

        public Texture2D NormalTx {
            get { return normalTx; }
            set { normalTx = value; }
        }
        public Texture2D HoverTx {
            get { return hoverTx; }
            set { hoverTx = value; }
        }
        public Texture2D PressedTx {
            get { return pressedTx; }
            set { pressedTx = value; }
        }

        #endregion

        public override void Draw(SpriteBatch spriteBatch) {

            Texture2D currentTx;

            switch (buttonState) {
                case ButtonState.Hovered:
                    if (hightlightType == HightlightType.ChangePicture)
                        currentTx = HoverTx;
                    else {
                        currentTx = normalTx;
                    }
                    break;
                case ButtonState.Pressed:
                    currentTx = PressedTx;
                    break;

                case ButtonState.Normal:
                default:
                    currentTx = NormalTx;
                    break;
            }

            spriteBatch.Draw(currentTx, ControlRectangle, null, color, rotation, Origin, ControlSpriteEffects, LayerDepth);
            if (hightlightType == HightlightType.Hightlight && buttonState == ButtonState.Hovered)
                spriteBatch.Draw(hoverTx, HightlightPosition + position, null, Color.White, rotation, Origin, scale, ControlSpriteEffects, LayerDepth + .001f);
        }

        #region Події

        public override void CallEvent_MouseHoverIn(Vector2 position) {
            buttonState = ButtonState.Hovered;
            base.CallEvent_MouseHoverIn(position);
        }
        public override void CallEvent_MouseHoverOut(Vector2 position) {
            buttonState = ButtonState.Normal;
            base.CallEvent_MouseHoverOut(position);
        }
        public override void CallEvent_MousePress(int key) {
            if (!KeyTriggers.Contains(key)) return;
            buttonState = ButtonState.Pressed;
            base.CallEvent_MousePress(key);
        }
        public override void CallEvent_MouseRelease(int key) {
            if (!KeyTriggers.Contains(key)) return;
            buttonState = ButtonState.Hovered;
            base.CallEvent_MouseRelease(key);
        }

        #endregion
    }
}