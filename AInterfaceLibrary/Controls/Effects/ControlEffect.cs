﻿using AInterfaceLibrary;
using Microsoft.Xna.Framework;

namespace AInterfaceLibrary.Controls.Effects
{
    public abstract class ControlEffect : GameComponent {
        public bool ToDelete;
        protected ControlEffect(Game game, Control target) : base(game) {
            Target = target;
            target.ControlEffects.Add(this);
            Enabled = true;
            game.Components.Add(this);
            ToDelete = false;
        }

        public void Remove() {
            Game.Components.Remove(this);
            ToDelete = true;
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
        }
        protected Control Target;
    }
}