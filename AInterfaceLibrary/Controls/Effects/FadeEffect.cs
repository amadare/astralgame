﻿
using Microsoft.Xna.Framework;

namespace AInterfaceLibrary.Controls.Effects
{
    public enum FadeEffectAfterEffect {
        None, Remove
    }
    public class FadeEffect : ControlEffect
    {
        private float speed;
        private bool loop;
        private bool increase;
        private float min;
        private float max;
        public FadeEffectAfterEffect AfterEffect;

        #region Конструктор

        public FadeEffect(Game game,Control target,
            float speed = 0.005f,
            bool loop = true,
            bool increase = true,
            float min = 0,
            float max = 1f)
            : base(game, target)
        {
            this.max = max;
            this.min = min;
            this.increase = increase;
            this.loop = loop;
            this.speed = speed;
            Enabled = true;
        }

        #endregion

        public override void Update(GameTime gameTime)
        {
            if (increase)
            {
                Target.Opacity += speed * gameTime.ElapsedGameTime.Milliseconds;
                if (Target.Opacity > max)
                {
                    Target.Opacity = max;
                    increase = false;
                    if (!loop)
                        Enabled = false;
                }

            }
            else
            {
                Target.Opacity -= speed * gameTime.ElapsedGameTime.Milliseconds;
                if (Target.Opacity < min)
                {
                    Target.Opacity = min;
                    increase = true;
                    if (!loop) {
                        Enabled = false;
                        if (AfterEffect==FadeEffectAfterEffect.Remove)
                            Target.Remove();
                    }
                }
            }
        }
    }
}