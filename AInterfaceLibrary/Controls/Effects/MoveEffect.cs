﻿using AInterfaceLibrary;
using Microsoft.Xna.Framework;

namespace AInterfaceLibrary.Controls.Effects {
    public class MoveEffect : ControlEffect {
        public enum OtherMoveEffectBehavior {
            DoNothing,
            Disable,
            Delete
        }
        private int time, delay, past, elapsed;
        private Vector2 p1, p2;
        private bool started;
        public OtherMoveEffectBehavior conflictBehavior = OtherMoveEffectBehavior.Delete;

        private void TurnOthersMoveEffect(bool state = false) {
            if (conflictBehavior==OtherMoveEffectBehavior.DoNothing) return;

            foreach (var effect in Target.ControlEffects) {
                if (effect is MoveEffect && effect != this) {
                    switch (conflictBehavior) {
                            case OtherMoveEffectBehavior.Disable:
                            effect.Enabled = state;
                            continue;
                            case OtherMoveEffectBehavior.Delete:
                            effect.ToDelete = true;
                            continue;
                    }
                    
                }
            }
        }

        public MoveEffect(Game game,Control target, int time, Vector2 position, int delay = 0)
            : base(game,target) {
            p1 = target.PositionVector;
            p2 = position;
            this.time = time;
            this.delay = delay;
            TurnOthersMoveEffect();
        }

        public MoveEffect(Game game,Control target, Vector2 position1, Vector2 position2, int time, int delay)
            : base(game,target) {
            p1 = position1;
            p2 = position2;
            this.time = time;
            this.delay = delay;
            past = 0;
            TurnOthersMoveEffect();
        }

        public override void Update(GameTime gameTime) {
                if (!started) {
                    past += gameTime.ElapsedGameTime.Milliseconds;
                    if (past >= delay) {
                        elapsed = past - delay;
                        started = true;
                    }
                } else {
                    elapsed += gameTime.ElapsedGameTime.Milliseconds;
                    float mod = elapsed / (float)time;
                    if (mod > 1)
                        mod = 1;
                    Target.PositionX = p1.X + ((p2.X - p1.X) * mod);
                    Target.PositionY = p1.Y + ((p2.Y - p1.Y) * mod);

                    if (mod == 1) {
                        this.Enabled = false;
                        this.ToDelete = true;
                    }
                }
        }
    }
}