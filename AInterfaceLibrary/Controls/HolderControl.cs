﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary.Controls {
    public class HolderControl : Control {

        public HolderControl(Game game, ControlManager manager)
            : base(game, manager) {
            //CursorTargetable = false;
        }
        public void UpdateSize() {
            foreach (Control child in ChildControls) {
                if (child.SizeX > size.X)
                    size.X = child.SizeX;
                if (child.SizeY > size.Y)
                    size.Y = child.SizeY;
            }
        }

        public override void Draw(SpriteBatch spriteBatch) {
        }

        public override void Update(GameTime gameTime) {
            
        }
    }
}