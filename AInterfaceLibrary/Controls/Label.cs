﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary.Controls {
    public class Label : Control {
        private string text;
        public string Text {
            get { return text; }
            set {
                text = value;
                size = spriteFont.MeasureString(text);
            }
        }
        public new Vector2 PositionVector {
            set { position = value;
                position.Y = (int) position.Y;
            }
            get { return position; }
        }
        public Label(Game game, ControlManager manager, string text, int X, int Y, Color color, SpriteFont fnt = null)
            : base(game, manager) {
            Text = text;
            PositionVector = new Vector2(X, Y);
            spriteFont = fnt ?? manager.SpriteFont;
            base.color = color;
            CursorTargetable = false;
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(spriteFont, Text, position + Origin, color, rotation, Origin, scale,
                                   ControlSpriteEffects, LayerDepth);
        }

        public override void Update(GameTime gameTime) {
        }

        public override string ToString() {
            return text;
        }
    }
}