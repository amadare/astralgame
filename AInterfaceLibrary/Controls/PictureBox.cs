﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary.Controls {
    public class PictureBox : Control {

        #region Поля і властивості

        private Texture2D texture;
        public enum DrawTypeEnum {
            Rectangle, Vector
        }

        public DrawTypeEnum DrawType;
        public Texture2D Texture { get { return texture; } set { texture = value; CalcSize(); } }
        public new float Scale { get { return scale; } set { scale = value; CalcSize(); } }

        #endregion

        #region Конструктори

        public PictureBox(Game game, ControlManager manager, Texture2D picture, Vector2 position)
            : base(game, manager) {
            texture = picture;
            this.position = position;
            CalcSize();
            DrawType = DrawTypeEnum.Vector;
        }

        public PictureBox(Game game, ControlManager manager, Texture2D picture, Rectangle position) :
            base(game, manager) {
            texture = picture;
            size.X = position.Width;
            size.Y = position.Height;
            this.position.X = position.X;
            this.position.Y = position.Y;
            DrawType = DrawTypeEnum.Rectangle;
        }

        #endregion

        private void CalcSize() {
            size.X = texture.Width * scale;
            size.Y = texture.Height * scale;
        }

        public override void Draw(SpriteBatch spriteBatch) {
            if (DrawType == DrawTypeEnum.Vector)
                spriteBatch.Draw(texture, position, null, ControlColor, rotation, Origin, scale, SpriteEffects.None, LayerDepth);
            else {
                spriteBatch.Draw(texture, ControlRectangle, null, ControlColor, 0f, Origin, SpriteEffects.None, LayerDepth);
            }
        }

        public override void Update(GameTime gameTime) {
            
        }
    }
}