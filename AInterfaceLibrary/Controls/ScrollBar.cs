﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using AInputLibrary;

namespace AInterfaceLibrary.Controls {

    public class ScrollBar : Control {

        #region Події

        public delegate void ValueChangedEventHandler(float value);
        public event ValueChangedEventHandler ValueChanged;

        #endregion

        #region Поля та властивості

        private Texture2D scrollButtonTexture, _backgroundTile, scrollBackground;
        private ScrollButton scrollButton;
        public int Lenght;
        private int pixelCount;
        private float value;
        public float Value {
            get { return value; }
            set {
                if (value >= 1) {
                    this.value = 1;
                    scrollButton.PositionY = scrollButton.maxY + PositionY;
                } else if (value <= 0) {
                    this.value = 0;
                    scrollButton.PositionY = scrollButton.minY + PositionY;
                } else {
                    this.value = value;
                    scrollButton.PositionY = (pixelCount * this.value) + PositionY;
                }
                if (ValueChanged != null)
                    ValueChanged(this.value);
            }
        }
        public enum ValueType {
            Normal, Max, Min
        }

        public new float LayerDepth {
            set {
                layerDepth = value;
                childControls[0].LayerDepth = value + .002f;
                childControls[1].LayerDepth = value + .002f;
                scrollButton.LayerDepth = value + .001f;
            }
            get { return layerDepth; }
        }

        #endregion

        #region Вкладений клас кнопки
        private class ScrollButton : Button {
            public event ButtonMoveHandler Move;
            public delegate void ButtonMoveHandler(int pixels, ValueType type);

            private bool Hold = false;
            public float maxY, minY;
            public ScrollButton(float maxY, float minY, Game game, ControlManager manager, float positionX, float positionY, Texture2D normalTx, Texture2D hoverTx = null, Texture2D pressedTx = null)
                : base(game, manager, positionX, positionY, normalTx, hoverTx, pressedTx) {
                this.minY = minY;
                this.maxY = maxY;

                this.MousePress += key => { Hold = true; };
                InputHandler.MouseButtonReleased +=
                    (key) => { Hold = false; };

                InputHandler.MouseMove += MouseMove;
            }

            private void MouseMove(Vector2 current, Vector2 old) {
                if (!Hold) return;
                float lastY = PositionY;

                ValueType type = ValueType.Normal;

                PositionY += current.Y - old.Y;
                if (PositionY > maxY + parentControl.PositionY) {
                    PositionY = maxY + parentControl.PositionY;
                    type = ValueType.Max;
                } else if (PositionY < minY + parentControl.PositionY) {
                    PositionY = minY + parentControl.PositionY;
                    type = ValueType.Min; ;
                }

                if (Move != null)
                    Move((int)(PositionY - lastY), type);
            }
        }
        #endregion

        #region Конструктор

        public ScrollBar(Game game, ControlManager manager, int lenght,
            Texture2D scrollButtonTexture, Texture2D backgroundTile, Texture2D buttonTexture, Texture2D endTexture)
            : base(game, manager) {
            this.scrollButtonTexture = scrollButtonTexture;
            this._backgroundTile = backgroundTile;
            Lenght = lenght;
            pixelCount = lenght - scrollButtonTexture.Height - endTexture.Height;
            scrollButton = new ScrollButton(lenght - scrollButtonTexture.Height - endTexture.Height, endTexture.Height, game, manager, 2, 6, scrollButtonTexture);
            Value = 0;
            scrollButton.Move += (pixels, val) => {
                switch (val) {
                    case ValueType.Normal:
                        Value += pixels / (float)pixelCount;
                        break;
                    case ValueType.Min:
                        Value = 0;
                        break;
                    case ValueType.Max:
                        Value = 1;
                        break;
                }
            };

            #region генеруємо текстуру
            int position = endTexture.Height;
            RenderTarget2D renderTarget = new RenderTarget2D(game.GraphicsDevice, backgroundTile.Width, lenght);
            game.GraphicsDevice.SetRenderTarget(renderTarget);
            SpriteBatch spriteBatch = new SpriteBatch(game.GraphicsDevice);
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque);
            spriteBatch.Draw(endTexture, Vector2.Zero, Color.White);
            while (position < lenght) {
                spriteBatch.Draw(backgroundTile, new Vector2(0, position), Color.White);
                position += backgroundTile.Height;
            }
            spriteBatch.Draw(endTexture, new Vector2(0, lenght - endTexture.Height), null, ControlColor, 0f, Vector2.Zero, scale, SpriteEffects.FlipVertically, LayerDepth + .0001f);
            spriteBatch.End();

            game.GraphicsDevice.SetRenderTarget(null);
            scrollBackground = renderTarget;
            #endregion

            var buttonUp = new Button(game, manager, 
                PositionX + backgroundTile.Width / 2 - buttonTexture.Width / 2,
                                      PositionY,
                                      buttonTexture) { LayerDepth = this.LayerDepth + 0.1f };

            var buttonDown = new Button(game, manager, 
                PositionX + backgroundTile.Width/2 - buttonTexture.Width/2,
                                        PositionY + lenght-endTexture.Height, 
                                        buttonTexture) {
                                            LayerDepth = this.LayerDepth + 0.1f
                                        };
            buttonDown.ControlSpriteEffects=SpriteEffects.FlipVertically;

            buttonUp.MouseRelease += key => {Value -= .1f;};
            buttonDown.MouseRelease += key => 
            { Value += .1f; };

            AddChild(buttonUp);
            AddChild(buttonDown);
            AddChild(scrollButton);
        }

        #endregion

        #region Перезапис віртуальних класів

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(scrollBackground, PositionVector, null, ControlColor, Rotation, Origin, Scale, SpriteEffects.None, LayerDepth);
            /*spriteBatch.DrawString(spriteFont,
                string.Format("Pos:{0}; Value:{1};Max:{2};Min:{3}.",
                scrollButton.PositionY,
                Value,
                scrollButton.maxY + scrollButton.ParrentControl.PositionY,
                scrollButton.minY + scrollButton.ParrentControl.PositionY),
                PositionVector, Color.Red, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);*/
        }

        #endregion

        /* SB = new ScrollBar(GameRef, controlManager, 186, Data.Textures.GetItem("ScrollBar"),
                          Data.Textures.GetItem("ScrollBarBackground"),Data.Textures.GetItem("ScrollBarButton"),Data.Textures.GetItem("ScrollBarEnd")) { LayerDepth = .9f };
            SB.PositionVector=new Vector2(300,200);*/
    }
}