﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AInterfaceLibrary.Controls {
    public class TextListControl : Control {
        private ScrollBar scrollBar;
        public List<string> Text;
        private int rowsCanType, startingRow;

        public new float LayerDepth {
            set {
                layerDepth = value;
                scrollBar.LayerDepth = value + .001f;
            }
            get { return layerDepth; }
        }

        public TextListControl(Game game, ControlManager manager,Vector2 position, Vector2 size, 
            Texture2D scrButton, Texture2D scrBackgr, Texture2D scrArrow,Texture2D scrEnd)
            : base(game, manager) {
            PositionVector = position;
            scrollBar=new ScrollBar(game,manager,(int)size.Y,scrButton,scrBackgr,scrArrow,scrEnd)
            {PositionVector = new Vector2(PositionX+size.X-scrBackgr.Width,PositionY)
            };
            Text = new List<string>();
            SizeVector = size;
            scrollBar.Value = 1f;
            rowsCanType = (int)(size.Y / spriteFont.LineSpacing);
            scrollBar.ValueChanged += CheckValue;
            CheckValue(scrollBar.Value);
        }

        void CheckValue(float value) {
            startingRow = (int)(Text.Count * value)-rowsCanType;
            if (startingRow < 0)
                startingRow = 0;
            /*if (startingRow >= rowsCanType) return;
            startingRow = Text.Count - rowsCanType;
            if (startingRow < 0)
                startingRow = 0;*/
        }

        public void AddString(string str) {
            Text.Add(str);
            CheckValue(scrollBar.Value);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            for (int i = startingRow; i < Text.Count && i-startingRow<=rowsCanType; i++) {
                spriteBatch.DrawString(spriteFont, Text[i], position + new Vector2(0, (i-startingRow) * spriteFont.LineSpacing),ControlColor,Rotation,Origin,Scale,ControlSpriteEffects,1f);
            }

        }
    }
}