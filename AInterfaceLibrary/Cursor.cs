﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AInterfaceLibrary {
    public class Cursor : GameComponent {
        #region Поля і Властивості

        static public Texture2D CursorTexture;
        static public Vector2 CursorPosition;

        private int left, up, right, down;
        #endregion

        #region Конструктор

        public Cursor(Game game, Rectangle border, Texture2D texture, bool setToCenter = true)
            : base(game) {
            //отримуємо текстуру
            CursorTexture = texture;

            //розраховуємо границі
            left = border.X;
            right = left + border.Width;
            up = border.Y;
            down = up + border.Height;
        }

        #endregion

        #region Методи XNA

        public override void Update(GameTime gameTime) {
            //отримуємо координати курсора windows
            MouseState state = Mouse.GetState();
            CursorPosition = new Vector2(state.X, state.Y);

            //не даємо ігровому курсору виходити за край екрану
            if (CursorPosition.X > right)
                CursorPosition.X = right;
            if (CursorPosition.X < left)
                CursorPosition.X = left;
            if (CursorPosition.Y > down)
                CursorPosition.Y = down;
            if (CursorPosition.Y < up)
                CursorPosition.Y = up;
        }

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(CursorTexture, CursorPosition, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1);
        }

        #endregion
    }
}
