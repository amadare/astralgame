using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using AInterfaceLibrary;
using AstralGame.Data;
using AstralGame.GameStates;
using ACore;
using AInputLibrary;


namespace AstralGame {
    public class AstralGameClass : Microsoft.Xna.Framework.Game {

        #region ���� � ����������

        GraphicsDeviceManager graphics;

        public ControlManager ControlManager;
        public Cursor cursor;
        public SpriteBatch spriteBatch;
        public GameStateManager gameStateManager;
        public Rectangle ScreenRectangle;
        public InputHandler inputHandler;

        public SpriteFont menuFont;

        public int Width { get { return ScreenRectangle.Width; } }
        public int Height { get { return ScreenRectangle.Height; } }

        #endregion

        #region �����������

        protected override void BeginRun() {
            var r = new MainMenuScreen(this, gameStateManager, spriteBatch);
            gameStateManager.ChangeState(r);
            this.Components.Add(gameStateManager);
            Components.Add(cursor);

            base.BeginRun();
        }

        public AstralGameClass() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            gameStateManager = new GameStateManager(this);
            //SetResolution(1280, 720);
            SetResolution(1368,784);
            //SetResolution(1024, 768);
            //SetResolution(640,480);
            //SetResolution(800,600);
            //SetResolution(900,600);
            inputHandler = new InputHandler(this);
        }

        #endregion

        #region ������ XNA

        protected override void Initialize() {
            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            new Strings();

            #region ��������
            new Textures(Content.Load<Texture2D>("error"));

            foreach (LoadStruct entry in Content.Load<List<LoadStruct>>("T2")) {
                Texture2D texture;

                try {
                    texture = Content.Load<Texture2D>(entry.value);
                } catch {
                    texture = null;
                }

                Data.Textures.AddItem(texture, entry.key);

                cursor = new Cursor(this,ScreenRectangle, Content.Load<Texture2D>("GUI\\cursor"));
            }
            #endregion


            foreach (var entry in Content.Load<List<LoadStruct>>("Strings")) {
                Strings.AddItem(entry.key, entry.value);
            }
            menuFont = Content.Load<SpriteFont>("fnt");
            ControlManager.DefaultSpriteFont = menuFont;

            CardControl.frame = Textures.GetItem("CardFrame");
            CardControl.Back = Data.Textures.GetItem("CardBack");

        }
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime) {

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);
        }

        #endregion

        #region ������

        public void SetResolution(int width, int height, bool fullscreen = false) {
            //��������� ����������� ������
            ScreenRectangle = new Rectangle(
                0,
                0,
                width,
                height);

            //������ ��������� ���������� ������ ��� ���
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;

            //������������� �������������/������� �����
            graphics.IsFullScreen = fullscreen;

            graphics.ApplyChanges();
        }

        #endregion
    }
}
