﻿using ACardGameEngine;
using AInterfaceLibrary;
using AInterfaceLibrary.Controls.Effects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame {
    public delegate void CardClickEventHandler(CardControl card);

    public enum TargetStateEnum {
        CardSelect, TargetSelect, PlaceSelect
    }

    public class CardControl : Control {

        public static CardControl SelectedCard;
        public static TargetStateEnum TargetState;
        public Card Card;
        private Texture2D picture;
        public static Texture2D frame, Back;
        private bool selected;

        public bool Selected {
            get { return selected; }
            set {
                selected = value;
                if (value) SelectedCard = this;
                else if (this == SelectedCard) SelectedCard = null;
            }
        }
        public bool OwnerActive;
        private float selectedOpacity;
        private bool increaseSelectedOpacity;

        public static event CardClickEventHandler CardClick, CardSacrefice;

        public CardControl(Game game, ControlManager manager, Card card, CardTextInfoControl infoControl = null)
            : base(game, manager) {

            #region Ініціалізація

            Card = card;
            picture = Data.Textures.GetItem(card.PictureName);
            increaseSelectedOpacity = true;
            selectedOpacity = .1f;
            LayerDepth = .9f;
            SizeX = frame.Width;
            SizeY = frame.Height;


            Visible = false;

            #endregion

            if (infoControl != null) {
                MouseHoverOut += pos => infoControl.CardInfo = null;
                this.MouseHoverIn += pos => { if (OwnerActive) infoControl.CardInfo = Card; };
            }

            MouseRelease += key => {
                if (OwnerActive)
                    switch (key) {
                        case 0:
                            CardClick(this);
                            return;
                        case 1:
                            if (CardSacrefice != null)
                                CardSacrefice(this);
                            return;
                    }
            };

            if (Card.Type == CardType.Creature) {
                ((CreatureCard)Card).AttackEvent += (crd, owner, enemy) => {
                    new MoveEffect(game, this, 400, new Vector2(PositionX, PositionY + 40)) { conflictBehavior = MoveEffect.OtherMoveEffectBehavior.DoNothing };
                    new MoveEffect(game, this, 400, PositionVector, 400);
                };
            }

            CardClick += control => {
                if (this != control)
                    this.Selected = false;
                else {
                    if (Card.Type == CardType.Wall || Card.Type == CardType.Creature) {
                        CreatureCard creature = (CreatureCard)Card;
                        if (creature.Position == -1)
                            Selected = !Selected;
                        else {
                            if (creature.HasAbility) {
                                //todo: ability
                            }
                        }
                    } else {
                        if (((SpellCard)Card).NeedTarget) {
                            Selected = !Selected;
                            if (Selected) 
                                //TargetState = 
                                    TargetState = TargetStateEnum.TargetSelect;// ? TargetStateEnum.CardSelect : TargetStateEnum.TargetSelect;
                        }
                    }
                }
            };
        }

        public override void Update(GameTime gameTime) {
            if (Selected) {
                if (increaseSelectedOpacity) {
                    selectedOpacity += 0.00025f * gameTime.ElapsedGameTime.Milliseconds;
                    if (selectedOpacity > .5f)
                        increaseSelectedOpacity = false;
                } else {
                    selectedOpacity -= 0.00025f * gameTime.ElapsedGameTime.Milliseconds;
                    if (selectedOpacity < .1f)
                        increaseSelectedOpacity = true;
                }
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            if (OwnerActive) {
                if (Card.Type == CardType.Creature || Card.Type == CardType.Wall) {
                    #region Карта створіння

                    //мана
                    spriteBatch.DrawString(spriteFont, Card.ManaCost.ToString(), position + new Vector2(73, -5),
                                           Color.Wheat, rotation, Origin, scale, SpriteEffects.None, LayerDepth + .0004f);

                    //життя
                    spriteBatch.DrawString(spriteFont, ((CreatureCard)Card).Health.ToString(),
                                           position + new Vector2(70, 80),
                                           Color.Wheat, rotation, Origin, scale, SpriteEffects.None, LayerDepth + .0004f);

                    //атака
                    spriteBatch.DrawString(spriteFont, ((CreatureCard)Card).Attack.ToString(),
                                           position + new Vector2(3, 80),
                                           Color.Wheat, rotation, Origin, scale, SpriteEffects.None, LayerDepth + .0004f);

                    //рамка
                    spriteBatch.Draw(frame, position, null, ControlColor, rotation, Origin, scale, SpriteEffects.None,
                                     LayerDepth + .0003f);

                    //картинка
                    spriteBatch.Draw(picture, position + new Vector2(2, 10), null, this.ControlColor, rotation, Origin,
                                     scale, SpriteEffects.None, LayerDepth + .0002f);

                    #endregion
                } else {
                    spriteBatch.Draw(picture, position, null, ControlColor, rotation, Origin, 1f, ControlSpriteEffects, LayerDepth);
                    spriteBatch.DrawString(spriteFont, Card.ManaCost.ToString(),
                                           position + new Vector2(55, 0),
                                           Color.Wheat, rotation, Origin, scale, SpriteEffects.None, LayerDepth + .0004f);
                }
                if (Selected) {
                    #region Ореол вибору
                    //рамка
                    if (Card.Type == CardType.Creature || Card.Type == CardType.Wall)
                        spriteBatch.Draw(frame, position - new Vector2(4), null, ControlColor * selectedOpacity, rotation,
                                         Origin,
                                         scale * 1.1f, SpriteEffects.None, LayerDepth + .00042f);

                    //картинка
                    spriteBatch.Draw(picture, position + new Vector2(-2, 6), null, this.ControlColor * selectedOpacity,
                                     rotation, Origin,
                                     scale * 1.1f, SpriteEffects.None, LayerDepth + .00041f);
                    #endregion
                }
            } else {
                //"сорочка"
                spriteBatch.Draw(Back, PositionVector, null, ControlColor, Rotation, Origin, Scale, ControlSpriteEffects, LayerDepth);
            }
        }

        public override string ToString() {
            return Card == null ? "Empty" : Card.Name;
        }
    }
}