﻿using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame {
    public class CardHolderControl : PictureBox {
        public CardHolderControl(Game game, ControlManager manager, Texture2D picture, Vector2 position)
            : base(game, manager, picture, position) {
            Empty = true;
        }

        public int Place;
        public bool OnTop, Empty;
    }
}