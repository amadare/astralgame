﻿using ACardGameEngine;
using AInterfaceLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame {
    public class CardTextInfoControl : Control {
        public Card CardInfo;
        public int TipTimeout;
        public static int DefaultTimeout=2000;
        public CardTextInfoControl(Game game, ControlManager manager) : base(game, manager) {
            TipTimeout = DefaultTimeout;
        }

        public override void Update(GameTime gameTime) {
            /*if (CardInfo!=null)
                TipTimeout -= gameTime.ElapsedGameTime.Milliseconds;
            if (TipTimeout < 0) {
                CardInfo = null;
                TipTimeout = DefaultTimeout;
            }*/
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch) {
            if (CardInfo != null) {
                spriteBatch.DrawString(spriteFont, Data.Strings.GetItem(CardInfo.Name), PositionVector, Color.Wheat, rotation, Origin, scale+0.2f,
                                       ControlSpriteEffects, LayerDepth);
                spriteBatch.DrawString(spriteFont, Data.Strings.GetItem(CardInfo.Description), PositionVector+new Vector2(0,spriteFont.LineSpacing*scale), Color.White, rotation, Origin, scale,
                                       ControlSpriteEffects, LayerDepth);
            }
        }
    }
}