﻿using ACardGameEngine;
using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame {
    public class PlayerDataBlockControl : Control {
        private Player player;
        private Rectangle AvatarPosition;

        public PlayerDataBlockControl(Game game, ControlManager manager, Player player, Vector2 position, Rectangle avatarPosition)
            : base(game, manager) {
            this.player = player;
            this.position = position;
            AvatarPosition = avatarPosition;
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(spriteFont,player.Name,PositionVector,Color.Wheat);
            spriteBatch.Draw(player.Avatar,AvatarPosition,null,Color.White,0f,Vector2.Zero,SpriteEffects.None, .9f);
            Vector2 position = new Vector2(PositionX,AvatarPosition.Height+this.PositionY+spriteFont.Spacing+20);
            spriteBatch.DrawString(spriteFont, Data.Strings.GetItem("Health"), position, Color.Wheat, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9f);
            spriteBatch.DrawString(spriteFont, Data.Strings.GetItem("Magic_Power"), position + new Vector2(0, spriteFont.LineSpacing),
                Color.Wheat, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9f);
            spriteBatch.DrawString(spriteFont, Data.Strings.GetItem("Mana"), position + new Vector2(0, spriteFont.LineSpacing * 2), 
                Color.Wheat, 0f, Vector2.Zero, 1f, SpriteEffects.None, .9f);

            spriteBatch.DrawString(spriteFont, player.Life.ToString(),position+new Vector2(100,0),Color.White,0f,Vector2.Zero,1f,SpriteEffects.None,1f);
            spriteBatch.DrawString(spriteFont, player.Power.ToString(), position + new Vector2(100, spriteFont.LineSpacing), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            spriteBatch.DrawString(spriteFont, player.Mana.ToString(), position + new Vector2(100, spriteFont.LineSpacing*2), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
        }
    }
}