﻿using System.Collections.Generic;

namespace AstralGame.Data {
    public class BaseDataClass<T> {
        protected static Dictionary<string, T> Items;
        private static T DefaultValue;

        protected BaseDataClass(T defaultValue) {
            Items = new Dictionary<string, T>();
            DefaultValue = defaultValue;
        }

        static public void AddItem(T item, string name) {
            if (Items.ContainsKey(name)) {
                Items[name] = item;
                return;
            }

            if (item==null)
                Items.Add(name,DefaultValue);
            else 
                Items.Add(name,item);
        }

        static public T GetItem(string name) {
            T item;
            if (Items.TryGetValue(name, out item))
                return item;
            else {
                return DefaultValue;
            }
        }

        static public void RemoveItem(string name) {
            Items.Remove(name);

        }

        static void Clear() {
            Items.Clear();
        }
    }

    
}