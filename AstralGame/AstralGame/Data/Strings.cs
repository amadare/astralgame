﻿using System.Collections.Generic;

namespace AstralGame.Data {
    public class Strings {
        private static Dictionary<string, string> items;
 
        public Strings()  {
            items = new Dictionary<string, string>();
        }

        static public string GetItem(string key) {
            string value;
            if (items.TryGetValue(key, out value))
                return value;
            return key;
        }

        static public void AddItem(string key, string value) {
            items.Add(key,value);
        }

        static public void RemoveItem(string key) {
            items.Remove(key);
        }

        static public void Clear() {
            items.Clear();
        }
    }
}