﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame.Data {
    public class Textures : BaseDataClass<Texture2D> {
        public Textures(Texture2D errorTexture)
            : base(errorTexture) {
        }
    }
}