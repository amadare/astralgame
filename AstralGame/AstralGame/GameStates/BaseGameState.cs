﻿using System.Collections.Generic;
using AInterfaceLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ACore;

namespace AstralGame.GameStates {
    public class BaseGameState : GameState {

        //силки для дочірніх класів
        protected AstralGameClass GameRef;
        public bool AllowMouse;

        #region Конструктор

        public BaseGameState(Game game, GameStateManager manager, SpriteBatch spriteBatch)
            : base(game, manager, spriteBatch) {
            GameRef = (AstralGameClass)game;
            AllowMouse = true;
        }

        #endregion

        #region Методи XNA

        public override void Update(GameTime gameTime) {
            GameRef.inputHandler.Update(gameTime);
            controlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {

            GameRef.spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

            DrawBeforeGui(spriteBatch);
            DrawGui(spriteBatch);
            DrawAfterGui(spriteBatch);
            DrawCursor(spriteBatch);

            GameRef.spriteBatch.End();

            base.Draw(gameTime);
        }

        #endregion

        #region Малювання

        public override void DrawCursor(SpriteBatch spriteBatch) {
            GameRef.cursor.Draw(spriteBatch);
        }

        #endregion
    }
}