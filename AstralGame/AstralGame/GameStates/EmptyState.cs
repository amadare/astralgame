﻿using ACore;
using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame.GameStates {
    public class EmptyState : BaseGameState {
        private int time;
        private Control main;
        private static int clicks;
        public EmptyState(Game game, GameStateManager manager, SpriteBatch spriteBatch) : base(game, manager,spriteBatch) {
            time = 0;
            AllowMouse = true;
        }

        Control CreateControl(Control parrent) {
            return null;
            /*Control x = new Button(0, 0, Data.Textures.GetItem("Button1_n"));
            parrent.AddChild(x);
            clicks++;
            x.MousePress += key => {
                if (key == MouseKey.Left)
                    CreateControl(x);
                else 
                    x.ParrentControl.RemoveChild(x);
            };

            return x;*/
        }

        protected override void LoadContent() {
            /*Control current = new Button(0, 0, Data.Textures.GetItem("Button1_n"));
            main = current;
            controlManager.AddControl(current);

            for (int i = 0; i < 10; i++) {
                current = CreateControl(current);
            }

            Button b = new Button(0, 0, Data.Textures.GetItem("Button1_n"));
            Button a = new Button(15, 0, Data.Textures.GetItem("Button1_n")){Opacity = 0.4f};
            b.AddChild(a);*/
            
            base.LoadContent();
        }

        public override void Update(GameTime gameTime) {
            /*main.Shift(0.01f * gameTime.ElapsedGameTime.Milliseconds, 0.004f * gameTime.ElapsedGameTime.Milliseconds);
            /*time += gameTime.ElapsedGameTime.Milliseconds;
            if (time > 1000) {
                time -= 1000;
                StateManager.PushState(new EmptyState(GameRef,StateManager));
            }*/
            base.Update(gameTime);
        }

        public override void DrawAfterGui(SpriteBatch spriteBatch) {
            //spriteBatch.DrawString(ControlManager.SpriteFont,clicks.ToString(),Vector2.Zero,Color.Red);
            base.DrawAfterGui(spriteBatch);
        }
    }
}