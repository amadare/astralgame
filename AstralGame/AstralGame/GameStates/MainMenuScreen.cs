﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ACardGameEngine;
using AInputLibrary;
using ACore;

using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using AInterfaceLibrary.Controls.Effects;

namespace AstralGame.GameStates{
    public class MainMenuScreen : BaseGameState {

        #region Поля

        private Texture2D backgroundImage;

        #endregion

        public MainMenuScreen(Game game, GameStateManager manager, SpriteBatch spriteBatch) : base(game, manager, spriteBatch) {
            InputHandler.InputCodes.Add(new InputArgs(InputDevice.Keyboard, Keys.Escape, false), 1);
        }

        #region Методи XNA

        protected override void LoadContent(){
            #region Кнопки/контроли
            new PictureBox(GameRef,controlManager,Data.Textures.GetItem("TitleBackground"), Vector2.Zero){LayerDepth = .1f};

            Button nGameButton;
                nGameButton = new Button(GameRef,controlManager,0, 0, Data.Textures.GetItem("firstbtn"),Data.Textures.GetItem("highlight")){LayerDepth = .200f};
            nGameButton.hightlightType=HightlightType.Hightlight;
            nGameButton.AddChild(new Label(GameRef, controlManager, Data.Strings.GetItem("New Game"), 32, 116, Color.Wheat) { LayerDepth = .201f });
            nGameButton.ChildControls[0].CenterOnParrent(true,false);
            nGameButton.HightlightPosition=new Vector2(16,116);
            nGameButton.Type = "New game";

            var Options = new Button(GameRef, controlManager, 0, 0, Data.Textures.GetItem("btn"), Data.Textures.GetItem("highlight")) { LayerDepth = .210f };
            Options.AddChild(new Label(GameRef, controlManager, Data.Strings.GetItem("Options"), 0, 0, Color.Wheat) { LayerDepth = .211f });
            Options.HightlightPosition=new Vector2(9,38);
            Options.hightlightType=HightlightType.Hightlight;
            Options.ChildControls[0].CenterOnParrent(true,true,0,13);
            Options.Type = "Options";

            var About = new Button(GameRef, controlManager, 0, 0, Data.Textures.GetItem("btn"), Data.Textures.GetItem("highlight")) { LayerDepth = .220f };
            About.AddChild(new Label(GameRef, controlManager, Data.Strings.GetItem("About Me"), 0, 0, Color.Wheat) { LayerDepth = .221f });
            About.HightlightPosition = new Vector2(9, 38);
            About.hightlightType = HightlightType.Hightlight;
            About.ChildControls[0].CenterOnParrent(true, true, 0, 13);
            About.Type = "About";

            var Exit = new Button(GameRef, controlManager, 0, 0, Data.Textures.GetItem("lbtn"), Data.Textures.GetItem("highlight")) { LayerDepth = .230f };
            Exit.AddChild(new Label(GameRef, controlManager, Data.Strings.GetItem("Exit"), 0, 0, Color.Wheat) { LayerDepth = .231f });

            Exit.hightlightType=HightlightType.Hightlight;
            Exit.HightlightPosition=new Vector2(9,38);
            Exit.ChildControls[0].CenterOnParrent(true,true,0,13);
            Exit.MouseRelease += key => GameRef.Exit();

            new MoveEffect(GameRef, nGameButton, new Vector2(0, -160), new Vector2(0, 0), 1000, 0);
            new MoveEffect(GameRef, Options, Vector2.Zero, new Vector2(8, 160), 800, 200);
            new MoveEffect(GameRef, About, Vector2.Zero, new Vector2(9, 246), 800, 200);

            new MoveEffect(GameRef, Exit, Vector2.Zero, new Vector2(8, 330), 900, 100);
            #endregion

            CardManager cardManager = new CardManager("ACardsLibrary.dll");

           

            nGameButton.MouseRelease += key => {
                var p1cards = cardManager.GetAllCards().ToList();
                Player p1 = new Player("Player1", new List<Card>(cardManager.GetAllCards()),
                                  Data.Textures.GetItem("[Card_Cr_Fire_Minotaur]")) {Power = 20, Mana = 20 };
                Player p2 = new Player("Player2", new List<Card>(cardManager.GetAllCards()),
                                       Data.Textures.GetItem("[Card_Cr_Fire_Minotaur]")) { Power = 20};
                StateManager.PushState(new PlayingState(p1, p2, true, GameRef, StateManager, GameRef.spriteBatch));
            };

            Options.MouseRelease +=
                key => StateManager.PushState(new OptionsState(GameRef, StateManager, GameRef.spriteBatch));

            base.LoadContent();
        }
        #endregion
    }
}