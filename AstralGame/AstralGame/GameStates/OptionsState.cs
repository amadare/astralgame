﻿using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame.GameStates {
    public class OptionsState : BaseGameState {
        private int height, width;
        private Label info;
        public OptionsState(Game game, GameStateManager manager, SpriteBatch spriteBatch)
            : base(game, manager, spriteBatch) {
            
        }

        protected override void LoadContent() {
            new PictureBox(GameRef, controlManager, Data.Textures.GetItem("TitleBackground"), Vector2.Zero){LayerDepth = 0f};

            info = new Label(GameRef,controlManager,"",0,0,Color.Red);
            Button ResolutionButton800x600 = new Button(GameRef, controlManager, 0, 50, Data.Textures.GetItem("Button1_n")){LayerDepth = 0.9f};
            ResolutionButton800x600.AddChild(new Label(GameRef, controlManager, "800 x 600", 0, 50, Color.Wheat){LayerDepth = 0.92f});
            ResolutionButton800x600.MouseRelease += key => { 
                height = 600;
                width = 800;
            };

            Button ResolutionButton1366x768 = new Button(GameRef, controlManager, 0, 150, Data.Textures.GetItem("Button1_n")) { LayerDepth = 0.9f };
            ResolutionButton1366x768.AddChild(new Label(GameRef, controlManager, "1366 x 768", 0, 150, Color.Wheat) { LayerDepth = 0.92f });
            ResolutionButton1366x768.MouseRelease += key => {
                height = 768;
                width = 1366;
            };

            Button Fullscreen = new Button(GameRef, controlManager, 0, 300, Data.Textures.GetItem("Button1_n")) { LayerDepth = 0.9f };
            Fullscreen.AddChild(new Label(GameRef, controlManager, "Fullscreen", 0, 300, Color.Wheat) { LayerDepth = 0.92f });
            Fullscreen.MouseRelease += key => {
                GameRef.SetResolution(width,height,true);
            };

            Button Windowed = new Button(GameRef, controlManager, 0, 350, Data.Textures.GetItem("Button1_n")) { LayerDepth = 0.9f };
            Windowed.AddChild(new Label(GameRef, controlManager, "Windowed", 0, 350, Color.Wheat) { LayerDepth = 0.92f });
            Windowed.MouseRelease += key => {
                GameRef.SetResolution(width, height, false);
            };

            Button Exit = new Button(GameRef, controlManager, 0, 450, Data.Textures.GetItem("Button1_n")) { LayerDepth = 0.9f };
            Exit.AddChild(new Label(GameRef, controlManager, "Exit", 0, 450, Color.Wheat) { LayerDepth = 0.92f });
            Exit.MouseRelease += key => {
                StateManager.PopState();
            };



            base.LoadContent();
        }

        public override void Update(GameTime gameTime) {
            info.Text = width.ToString() + "x" + height.ToString();
            base.Update(gameTime);
        }


    }
}