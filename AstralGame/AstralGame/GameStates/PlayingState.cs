﻿using System.Collections.Generic;
using System.Diagnostics;
using ACardGameEngine;
using ACore;
using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using AInterfaceLibrary.Controls.Effects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AstralGame.GameStates {
    public class PlayingState : BaseGameState {
        #region UI-компоненти

        private PictureBox background, panel;
        public Vector2 ButtonsPosition, CardPlaceHolderPosition, Player1CardsPosition, Player2CardsPosition;
        private Rectangle TableRectangle;
        private int MenuHidePoint;
        private Button mainMenuBurron, optionsButton, giveupButton, nextTurnButton;
        private HolderControl buttonsHolder, tableSlotsHolder;
        private SpriteFont font;
        private bool needToChangeTurn;

        #endregion

        #region Ігрові компоненти

        private CardTextInfoControl infoControl;
        private PlayerDataBlockControl playerDataBlock1, playerDataBlock2;
        private CardGameProvider gameProvider;
        private PlayerCardControlManager player1ui, player2ui;
        private PlayerCardControlManager currentPlayerUi { get { return player1ui.player == gameProvider.CurrentPlayer ? player1ui : player2ui; } }

        #endregion

        #region Конструктор

        public PlayingState(Player currentPlayer, Player enemyPlayer, bool getCards, Game game, GameStateManager manager, SpriteBatch spriteBatch)
            : base(game, manager, spriteBatch) {

            #region розрахунок адаптативної верстки

            int x, y, w, h, rx, by;
            x = (int)(GameRef.ScreenRectangle.Width * 0.25);
            y = (int)(GameRef.ScreenRectangle.Height * 0.03);
            rx = (int)(GameRef.ScreenRectangle.Width * 0.03);
            by = (int)(GameRef.ScreenRectangle.Height * 0.25);
            w = (GameRef.ScreenRectangle.Width - x - rx);
            h = (GameRef.ScreenRectangle.Height - y - by);
            TableRectangle = new Rectangle(x, y, w, h);

            ButtonsPosition = new Vector2(GameRef.ScreenRectangle.Width - 195, 100);
            MenuHidePoint = GameRef.ScreenRectangle.Width - 50;

            CardPlaceHolderPosition = new Vector2((TableRectangle.Width - 558) / 2 + TableRectangle.X, (TableRectangle.Height - 203) / 2 + TableRectangle.Y);
            Player1CardsPosition = CardPlaceHolderPosition + new Vector2(0, 113 * 2);
            Player2CardsPosition = CardPlaceHolderPosition + new Vector2(0, -113);

            #endregion

            #region Ініціалізація ігрових компонентів

            gameProvider = new CardGameProvider(currentPlayer, enemyPlayer);
            infoControl = new CardTextInfoControl(GameRef, controlManager) {
                PositionVector = new Vector2(GameRef.ScreenRectangle.Width * .52f, GameRef.ScreenRectangle.Height * .78f),
                LayerDepth = .9f
            };

            if (getCards) {
                while (currentPlayer.Hand.Count < 6) {
                    currentPlayer.DrawCard();
                }
                while (enemyPlayer.Hand.Count < 5) {
                    enemyPlayer.DrawCard();
                }
            }

            playerDataBlock1 = new PlayerDataBlockControl(GameRef, controlManager, enemyPlayer, new Vector2(GameRef.Width * .054f, GameRef.Height * .1f),
                new Rectangle((int)(GameRef.Width * .04f), (int)(GameRef.Height * .11f), ((int)(GameRef.Width * 0.115)), ((int)(GameRef.Height * 0.158))));

            playerDataBlock2 = new PlayerDataBlockControl(GameRef, controlManager, currentPlayer, new Vector2(GameRef.Width * .054f, GameRef.Height * .42f),
                new Rectangle((int)(GameRef.Width * .04f), (int)(GameRef.Height * .425f), ((int)(GameRef.Width * 0.115)), ((int)(GameRef.Height * 0.158))));

            player1ui = new PlayerCardControlManager(currentPlayer, game, controlManager,
                infoControl, true, Player1CardsPosition, CardPlaceHolderPosition + new Vector2(0, 110));
            player2ui = new PlayerCardControlManager(enemyPlayer, game, controlManager,
                infoControl, false, Player2CardsPosition, CardPlaceHolderPosition);


            TextListControl textListControl = new TextListControl(GameRef, controlManager,
                                                                  new Vector2(.093f * GameRef.Width, .76f * GameRef.Height),
                                                                  new Vector2(.39f * GameRef.Width, .22f * GameRef.Height),
                                                                  Data.Textures.GetItem("ScrollBar"),
                                                                  Data.Textures.GetItem("ScrollBarBackground"),
                                                                  Data.Textures.GetItem("ScrollBarButton"),
                                                                  Data.Textures.GetItem("ScrollBarEnd"));
            textListControl.LayerDepth = .4f;

            PlayerCardControlManager.LogControl = textListControl;

            #endregion

            #region Підписки на події

            CardControl.CardSacrefice += CardSacrefice;
            CardControl.CardClick += CardClick;

            #endregion
        }

        #endregion

        private Label a, b;

        #region Методи, що оброблюють івенти

        public void CardSacrefice(CardControl cardControl) {
            gameProvider.Sacrefice(cardControl.Card);
        }
        public void CardClick(CardControl cardControl) {
            if (cardControl.Card is SpellCard)
                if (!((SpellCard)cardControl.Card).NeedTarget)
                    gameProvider.CastSpell((SpellCard)cardControl.Card);
                else {
                    CardControl.TargetState=TargetStateEnum.CardSelect;
                }
        }

        #endregion

        protected override void LoadContent() {

            #region сетап користувацького інтерфейсу

            #region кнопки бокового меню

            //picturebox для фону
            background = new PictureBox(GameRef, controlManager, Data.Textures.GetItem("back1"), new Vector2(GameRef.ScreenRectangle.Width / 2, GameRef.ScreenRectangle.Height / 2));
            background.Scale = GameRef.ScreenRectangle.Width / 362.5f;
            background.Origin = new Vector2(256, 256);
            background.CursorTargetable = false;
            background.LayerDepth = 0f;

            //кнопка головного меню
            mainMenuBurron = new Button(GameRef, controlManager, ButtonsPosition.X, ButtonsPosition.Y, Data.Textures.GetItem("gm_btn_normal"), Data.Textures.GetItem("gm_btn_hover"), Data.Textures.GetItem("gm_btn_pressed"));
            mainMenuBurron.AddChild(new Label(GameRef, controlManager, "Головне меню", 0, 0, Color.Wheat) { LayerDepth = .31f });
            mainMenuBurron.ChildControls[0].CenterOnParrent();
            mainMenuBurron.Type = "Main Menu button";
            mainMenuBurron.LayerDepth = .3f;

            //кнопка опцій
            optionsButton = new Button(GameRef, controlManager, ButtonsPosition.X, ButtonsPosition.Y + 43, Data.Textures.GetItem("gm_btn_normal"), Data.Textures.GetItem("gm_btn_hover"), Data.Textures.GetItem("gm_btn_pressed"));
            optionsButton.AddChild(new Label(GameRef, controlManager, "Опції", 0, 0, Color.Wheat) { LayerDepth = .31f });
            optionsButton.ChildControls[0].CenterOnParrent();
            optionsButton.Type = "Options";
            optionsButton.LayerDepth = .3f;

            //кнопка здаці
            giveupButton = new Button(GameRef, controlManager, ButtonsPosition.X, ButtonsPosition.Y + 86, Data.Textures.GetItem("gm_btn_normal"), Data.Textures.GetItem("gm_btn_hover"), Data.Textures.GetItem("gm_btn_pressed"));
            giveupButton.AddChild(new Label(GameRef, controlManager, "Здатись", 0, 0, Color.Wheat) { LayerDepth = .31f });
            giveupButton.ChildControls[0].CenterOnParrent();
            giveupButton.Type = "Give Up";
            giveupButton.LayerDepth = .3f;

            //кнопка передачі ходу
            nextTurnButton = new Button(GameRef, controlManager, ButtonsPosition.X + 10, ButtonsPosition.Y + 129, Data.Textures.GetItem("gm_lastbtn_normal"), Data.Textures.GetItem("gm_btn_lasthover"), Data.Textures.GetItem("gm_lastbtn_pressed"));
            nextTurnButton.AddChild(new Label(GameRef, controlManager, "Передати хід", 0, 0, Color.Wheat) { LayerDepth = .31f });
            nextTurnButton.ChildControls[0].CenterOnParrent(true, true, 20, -6);
            nextTurnButton.Type = "Skip Turn";
            nextTurnButton.LayerDepth = .3f;

            //контрол-утримувач кнопок
            buttonsHolder = new HolderControl(GameRef, controlManager);
            buttonsHolder.PositionVector = ButtonsPosition;

            buttonsHolder.AddChild(mainMenuBurron);
            buttonsHolder.AddChild(optionsButton);
            buttonsHolder.AddChild(giveupButton);
            buttonsHolder.AddChild(nextTurnButton);
            buttonsHolder.SizeX = 200;
            buttonsHolder.SizeY = 260;
            buttonsHolder.LayerDepth = 2f;

            new MoveEffect(GameRef, buttonsHolder, 700, new Vector2(MenuHidePoint, 100));
            buttonsHolder.MouseHoverOut +=
                (cursor) => new MoveEffect(GameRef, buttonsHolder, 400, new Vector2(MenuHidePoint, 100));
            buttonsHolder.MouseHoverIn +=
                (cursor) => new MoveEffect(GameRef, buttonsHolder, 400, new Vector2(ButtonsPosition.X, 100));

            #endregion

            #region Інфо по гравцям
            #endregion

            #region інші контроли

            //picturebox для інтерфейсу
            panel = new PictureBox(GameRef, controlManager, Data.Textures.GetItem("game_panel"), GameRef.ScreenRectangle);
            panel.LayerDepth = 0.33f;

            #endregion

            #region підписки

            nextTurnButton.MouseRelease += key => {
                gameProvider.NextTurn();
                player1ui.ChangeTurn();
                player2ui.ChangeTurn();
            };

            #endregion

            //debug
            mainMenuBurron.MouseRelease += key => StateManager.PopState();
            optionsButton.MouseRelease += key => StateManager.PushState(new OptionsState(GameRef, StateManager, GameRef.spriteBatch));
            giveupButton.MouseRelease += key => gameProvider.Player1.DrawCard();

            a = new Label(GameRef, controlManager, "None", 0, 0, Color.Red) { LayerDepth = 1f };
            b = new Label(GameRef, controlManager, "None", 0, 70, Color.Red) { LayerDepth = 1f };

            #region Слоти під карти

            //558x203

            tableSlotsHolder = new HolderControl(GameRef, controlManager);
            tableSlotsHolder.PositionVector = CardPlaceHolderPosition;

            for (int i = 0; i < 6; i++) {
                CardHolderControl tmp = new CardHolderControl(GameRef, controlManager, Data.Textures.GetItem("CardSlot"),
                                                CardPlaceHolderPosition + new Vector2(93 * i, 0)) {
                                                    ControlColor = Color.Red,
                                                    Opacity = 0.3f,
                                                    LayerDepth = .2f,
                                                    Place = i,
                                                    OnTop = true
                                                };
                tableSlotsHolder.AddChild(tmp);
                tmp.MouseHoverIn += (cursor) => { tmp.Opacity = 0.5f; };
                tmp.MouseHoverOut += (cursor) => { tmp.Opacity = 0.3f; };
                tmp.MouseRelease += key => CardHolderClicked(tmp.Place, tmp.PositionVector);
            }
            for (int i = 0; i < 6; i++) {
                CardHolderControl tmp = new CardHolderControl(GameRef, controlManager, Data.Textures.GetItem("CardSlot"),
                                                CardPlaceHolderPosition + new Vector2(93 * i, 110)) {
                                                    ControlColor = Color.LawnGreen,
                                                    Opacity = 0.3f,
                                                    LayerDepth = .2f,
                                                    Place = i,
                                                    OnTop = false
                                                };
                tableSlotsHolder.AddChild(tmp);
                tmp.MouseHoverIn += (cursor) => { tmp.Opacity = 0.5f; };
                tmp.MouseHoverOut += (cursor) => { tmp.Opacity = 0.3f; };
                tmp.MouseRelease += key => CardHolderClicked(tmp.Place, tmp.PositionVector);
            }
            #endregion

            #endregion

            base.LoadContent();
        }

        //призивання карти
        void CardHolderClicked(int position, Vector2 vector) {
            if (CardControl.SelectedCard != null) {
                if (CardControl.SelectedCard.Card.Type == CardType.Creature ||
                    CardControl.SelectedCard.Card.Type == CardType.Wall) {
                    if (gameProvider.SummonCreature((CreatureCard)CardControl.SelectedCard.Card, position)) {
                        currentPlayerUi.Summon(CardControl.SelectedCard, position);
                    }
                } else {
                    if (CardControl.TargetState==TargetStateEnum.TargetSelect)
                        gameProvider.CastSpell((SpellCard)CardControl.SelectedCard.Card, gameProvider.EnemyPlayer.Table[position]);
                    //CardControl.TargetState = TargetStateEnum.CardSelect;
                }
            }
        }

        public override void Update(GameTime gameTime) {
            //поворот фону
            background.Rotation += 0.0001f * gameTime.ElapsedGameTime.Milliseconds;
            a.Text = (CardControl.SelectedCard == null ? "None" : CardControl.SelectedCard.Card.Name) + "\r\n" + gameProvider.CurrentPlayer.Name;
            b.Text = gameProvider.Atatking.ToString();

            gameProvider.Update(gameTime);

            base.Update(gameTime);
        }
    }
}