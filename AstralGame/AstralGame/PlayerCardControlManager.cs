﻿using System;
using System.Collections.Generic;
using System.Linq;
using ACardGameEngine;
using AInterfaceLibrary;
using AInterfaceLibrary.Controls;
using AInterfaceLibrary.Controls.Effects;
using Microsoft.Xna.Framework;

namespace AstralGame {
    public class PlayerCardControlManager {
        public Player player;
        public List<CardControl> Cards;
        public Vector2 HandVector, TableVector;
        private Game game;
        private ControlManager controlManager;
        private CardTextInfoControl infoControl;
        static public TextListControl LogControl;
        private int shift;


        public PlayerCardControlManager(Player player, Game game, ControlManager manager,
            CardTextInfoControl infoControl, bool active, Vector2 handPosition, Vector2 tablePosition) {

            HandVector = handPosition;
            TableVector = tablePosition;
            this.player = player;
            this.game = game;
            controlManager = manager;
            this.infoControl = infoControl;

            Cards = new List<CardControl>();

            //додаємо нові карти з руки
            int cardNum = 0;
            foreach (Card card in player.Hand) {
                Cards.Add(new CardControl(game, manager, card, infoControl) {
                    PositionVector = HandVector + new Vector2(93 * cardNum, 0),
                    OwnerActive = active
                });
                cardNum++;
            }

            //додаємо карти з столу
            for (int i = 0; i < 6; i++)
                if (player.Table[i] != null) {
                    Cards.Add(new CardControl(game, controlManager, player.Table[i], infoControl) {
                        PositionVector = new Vector2(TableVector.X * i, TableVector.Y),
                        OwnerActive = true
                    });
                }

            //піписуємся на івенти
            player.CardAdded += CardAdded;
            player.CardRemoved += CardRemoved;
            player.CastEvent +=  SpellCardCasted;
        }

        private void CardAdded(Card card, Player owner, Player enemy) {
            var control = new CardControl(game, controlManager, card, infoControl) {
                PositionVector = new Vector2(game.Window.ClientBounds.Width, game.Window.ClientBounds.Height),
                OwnerActive = player==owner
            };
            Vector2 position;
            Cards.Add(control);
            if (card.Position == -1)
                position = HandVector + new Vector2((player.Hand.Count - 1) * 93, 0);
            else
                position = TableVector + new Vector2(card.Position * 93, 0);

            new MoveEffect(game, control, 500, position);
        }

        private void SpellCardCasted(Card card, Player owner, Player enemy) {
            CardControl control = Cards.FirstOrDefault(cardControl => cardControl.Card == card);
            if (control == null) return;
            AdgustCardsPositions(control.PositionX);
            Cards.Remove(control);

            new FadeEffect(game, control, 0.002f, false, false) { AfterEffect = FadeEffectAfterEffect.Remove };
            new MoveEffect(game, control, 500, new Vector2(300, 300)){conflictBehavior = MoveEffect.OtherMoveEffectBehavior.DoNothing};
            LogControl.AddString(String.Format(Data.Strings.GetItem("CastString"), owner.Name, Data.Strings.GetItem(card.Name)));
        }

        private void CardRemoved(Card card, Player owner, Player enemy) {

            CardControl control = Cards.FirstOrDefault(cardControl => cardControl.Card==card);

            AdgustCardsPositions(control.PositionX);
            Cards.Remove(control);
            new FadeEffect(game, control, 0.004f, false, false) {AfterEffect = FadeEffectAfterEffect.Remove};
        }

        public CardControl CreateCardControl(Card card, Vector2 position) {
            CardControl cardControl = new CardControl(game, controlManager, card, infoControl) { PositionVector = position };
            return cardControl;
        }

        public void ChangeTurn() {
            foreach (CardControl cardControl in Cards) {
                if (cardControl.Card.Position == -1) {
                    cardControl.OwnerActive = !cardControl.OwnerActive;
                }
            }
        }

        private void AdgustCardsPositions(float pos) {
            foreach (CardControl control in Cards) {
                if (control.PositionX > pos && control.Card.Position == -1)
                    new MoveEffect(game, control, 500, new Vector2(control.PositionX - 93, control.PositionY));
            }
        }

        public void Summon(CardControl cardControl, int position) {
            var prevPos = cardControl.PositionX;
            AdgustCardsPositions(prevPos);
            new MoveEffect(game, cardControl, 500, TableVector + new Vector2(position * 93, 0));
            cardControl.Selected = false;
            LogControl.AddString(String.Format(Data.Strings.GetItem("SummonString"), player.Name, Data.Strings.GetItem(cardControl.Card.Name)));
        }
    }
}