using System;

namespace AstralGame {
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (AstralGameClass game = new AstralGameClass())
            {
                game.Run();
            }
        }
    }
#endif
}

