﻿using System.Collections.Generic;
using ACardGameEngine;
using ACore;
using AInterfaceLibrary;
using AInterfaceLibrary.Controls.Effects;
using Microsoft.Xna.Framework;

namespace AstralGame {
    //TODO: переробити під обджект пулл
    public class UI_Player {
        public List<CardControl> Hand;
        public CardControl[] Table;
        private Vector2 handVector, tableVector;
        private bool active;
        public bool Active {
            set {
                active = value;
                foreach (CardControl control in Hand) {
                    control.OwnerActive = value;
                }
            }
            get { return active; }
        }

        public UI_Player(Vector2 handVector, Vector2 tableVector) {
            this.handVector = handVector;
            this.tableVector = tableVector;
            Hand = new List<CardControl>();
            Table = new CardControl[6];
        }

        public void NextTurn() {
            foreach (CardControl control in Hand) {
                control.Selected = false;
            }
        }

        public void AddCardToHandByControl(CardControl card) {
            card.PositionVector = handVector + new Vector2(93 * Hand.Count, 0);
            Hand.Add(card);
            card.Visible = true;
        }
        public void AddCardToHand(Game game, ControlManager manager, Card card, bool active, CardTextInfoControl infoControl = null) {
            var control = new CardControl(game, manager, card, infoControl);
            control.PositionVector = handVector + new Vector2(93 * Hand.Count, 0);
            Hand.Add(control);
            control.Visible = true;
            control.OwnerActive = active;
        }

        public void AddCardToTable(Game game, ControlManager manager, Card card, CardTextInfoControl infoControl = null) {
            int position = ((CreatureCard)card).Position;
            var control = new CardControl(game, manager, card, infoControl);
            control.PositionVector = tableVector + new Vector2(93 * position, 0);
            Table[position] = control;
            control.Visible = true;
        }
        public void AddCardToTableByControl(CardControl card, CardGameProvider provider) {
            int position = ((CreatureCard)card.Card).Position;
            card.PositionVector = tableVector + new Vector2(93 * position, 0);
            Table[position] = card;
            card.Visible = true;
        }

        public void SummonCard(Game game, CardControl cardControl, int position, Vector2 vector) {
            Table[position] = cardControl;
            Hand.Remove(cardControl);
            new MoveEffect(game, cardControl, 500, vector);
            cardControl.Selected = false;
        }

        public void RemoveCardControl(Card card) {
            for (int i = 0; i < Hand.Count; i++)
                if (Hand[i].Card == card) {
                    Hand[i].Remove();
                    Hand.RemoveAt(i);
                    return;
                }

            for (int i = 0; i < Table.Length; i++)
                if (Table[i] != null)
                    if (Table[i].Card == card) {
                        Table[i] = null;
                        return;
                    }
        }
    }
}