﻿using System.Collections;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ARpgLibrary.GUI.Controls;
using ARpgLibrary.GUI.Controls.Effects;
using ARpgLibrary.Input;

namespace ARpgLibrary.GUI {
    public class ControlManager : List<Control> {
        #region Поля і Властивості

        private static SpriteFont spriteFont;
        public static SpriteFont SpriteFont {
            get { return spriteFont; }
            set { spriteFont = value; }
        }

        public ControlLayerManager layerManager;

        private int selectedControl = 0;

        #endregion

        #region Конструктори

        public ControlManager(SpriteFont font) {
            SpriteFont = font;
            layerManager = new ControlLayerManager();
        }

        public ControlManager(SpriteFont font, int capacity)
            : base(capacity) {
            SpriteFont = font;
            layerManager = new ControlLayerManager();
        }

        public ControlManager(SpriteFont font, IEnumerable<Control> collection)
            : base(collection) {
            SpriteFont = font;
            layerManager = new ControlLayerManager();
        }

        #endregion

        #region Методи

        public void Update(GameTime gameTime) {
            if (Count == 0)
                return;

            foreach (Control c in this) {
                if (c.Enabled) {

                    foreach (ControlEffect effect in c.ControlEffects) {
                        if (effect.Enabled)
                            effect.Update(gameTime);
                    }
                    c.Update(gameTime);
                }

                //if (c.HasFocus)
                //    c.HandleInput();
            }
        }

        public void Draw(SpriteBatch spriteBatch) {
            foreach (Control c in this)
                if (c.Visible)
                    c.Draw(spriteBatch);
        }

        public void NextControl() {
            if (Count == 0)
                return;

            int currentControl = selectedControl;

            this[selectedControl].HasFocus = false;

            do {
                selectedControl++;

                if (selectedControl == Count)
                    selectedControl = 0;

                if (this[selectedControl].TabStop && this[selectedControl].Enabled)
                    break;
            } while (currentControl != selectedControl);

            this[selectedControl].HasFocus = true;
        }

        public void PreviousControl() {
            if (Count == 0)
                return;

            int currentControl = selectedControl;

            do {
                selectedControl--;

                if (selectedControl < 0)
                    selectedControl = Count - 1;

                if (this[selectedControl].TabStop && this[selectedControl].Enabled)
                    break;
            } while (selectedControl != currentControl);

            this[selectedControl].HasFocus = true;
        }

        public void SelectControl(Control control) {
            int index = this.IndexOf(control);
            this[selectedControl].HasFocus = false;
            this[index].HasFocus = true;
        }

        public Control GetControl(Vector2 position){
            for (int i = ControlLayerManager.LayerCount-1; i >= 0; i--)
                foreach (Control control in ControlLayerManager.Layers[i])
                {
                    if (control.Intersects(position))
                        return control;
                }
            return null;
        }

        #endregion
    }
}